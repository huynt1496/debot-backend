const rp = require("request-promise");
const cheerio = require("cheerio");
const puppeteer = require('puppeteer');

const URL = 'https://www.binhthuan.gov.vn/';
// const bodURL = 'https://www.binhthuan.gov.vn/1313/32097/65383/thong-tin-gioi-thieu-lanh-dao-so';
const structureURL = 'https://www.binhthuan.gov.vn/1313/32099/63771/564199/chuc-nang-nhiem-vu/chuc-nang-nhiem-vu-quyen-han-co-cau-to-chuc-va-moi-quan-he-cong-tac-cua-so-giao-duc-va-dao-tao-b.aspx';
const wsURL = 'https://sgddt.binhthuan.gov.vn/1313/32097/58986/lich-tiep-cong-dan';
const ntURL = 'https://sgddt.binhthuan.gov.vn/1313/32097/58984/thong-bao';
const newsURL = 'https://www.binhthuan.gov.vn/1313/32097/65761/van-phong-so';

const options = async (url) => {
  const browser = await puppeteer.launch();
  const newPage = await browser.newPage();

  await newPage.goto(url);

  const datatotalPage = await newPage.evaluate(() => document.body.outerHTML);
  var $ = cheerio.load(datatotalPage);

  await browser.close();

  return $;
};

const bodCrawler = async () => {
  const dataDigContent = {
    data: `<h3>Không có dữ liệu</h3>`,
    type: "html",
  };
  return dataDigContent;
};

const structureCrawler = async () => {
  try {
    var $ = await options(structureURL);
  } catch (error) {
    return error;
  }
  const digContent = $("div.ArticleContent ");

  const dataDigContent = {
    data: digContent.html(),
    type: "html",
  };
  return dataDigContent;
};

const wsCrawler = async () => {
  try {
    var $ = await options(wsURL);
  } catch (error) {
    return error;
  }

  const wsContent = $("#ctrl_183690_30_lvwArticleInMenu_itemPlaceholderContainer > li");
  var page_ws = {};
  let data = [];
  for (let i = 0; i < wsContent.length; i++) {
    let avatar = URL + $(`#ctrl_183690_30_lvwArticleInMenu_itemPlaceholderContainer > li:nth-child(${i + 1}) > a > img`).attr('src');
    const title = $(`#ctrl_183690_30_lvwArticleInMenu_itemPlaceholderContainer > li:nth-child(${i + 1}) h2 > a`).text().trim();
    let URL_title = URL + $(`#ctrl_183690_30_lvwArticleInMenu_itemPlaceholderContainer > li:nth-child(${i + 1}) > h2 > a`).attr('href');
    data.push({
      title,
      URL_title,
      avatar,
    });
  }

  data.push({
    title: 'Xem thêm',
    URL_title: wsURL,
  });

  page_ws = {
    1: data,
  }
  return page_ws;
};

const notificationCrawler = async () => {
  try {
    var $ = await options(ntURL);
  } catch (error) {
    return error;
  }

  const notificationContent = $("#ctrl_183690_30_lvwArticleInMenu_itemPlaceholderContainer > li");
  var page_notification = {};
  let data = [];
  for (let i = 0; i < notificationContent.length; i++) {
    const data_avatar = $(`#ctrl_183690_30_lvwArticleInMenu_itemPlaceholderContainer > li:nth-child(${i + 1}) > a > img`);
    const title = $(`#ctrl_183690_30_lvwArticleInMenu_itemPlaceholderContainer > li:nth-child(${i + 1}) h2 > a`).text().trim();
    let URL_title = URL + $(`#ctrl_183554_30_lvwArticleInMenu_itemPlaceholderContainer > li:nth-child(${i + 1}) > h2 > a`).attr('href');
    const avatar = `${URL}${data_avatar.attr('src')}`;
    data.push({
      avatar,
      title,
      URL_title,
    });
  }
  data.push({
    title: 'Xem thêm',
    URL_title: ntURL,
  });

  page_notification = {
    1: data,
  }
  return page_notification;
};

const newsCrawler = async () => {
  try {
    var $ = await options(newsURL);
  } catch (error) {
    return error;
  }

  const newsContent = $("#ctrl_183690_30_lvwArticleInMenu_itemPlaceholderContainer > li");
  var page_news = {};
  let data = [];
  for (let i = 0; i < newsContent.length; i++) {
    const data_avatar = $(`#ctrl_183690_30_lvwArticleInMenu_itemPlaceholderContainer > li:nth-child(${i + 1}) > a > img`);
    const title = $(`#ctrl_183690_30_lvwArticleInMenu_itemPlaceholderContainer > li:nth-child(${i + 1}) h2 > a`).text().trim();
    let URL_title = URL + $(`#ctrl_183690_30_lvwArticleInMenu_itemPlaceholderContainer > li:nth-child(${i + 1}) > h2 > a`).attr('href');
    const des = $(`#ctrl_183690_30_lvwArticleInMenu_itemPlaceholderContainer > li:nth-child(${i + 1}) > div`).text().trim();
    const avatar = `${URL}${data_avatar.attr('src')}`;
    data.push({
      avatar,
      title,
      URL_title,
      des,
    });
  }
  data.push({
    title: 'Xem thêm',
    URL_title: newsURL,
  });
  page_news = {
    1: data,
  }

  return page_news;
};

const crawlDataBinhThuan = async (req, res) => {
  const { contentPage, page } = req?.body;
  if (!contentPage) {
    const results = await Promise.all([bodCrawler(), structureCrawler(), wsCrawler(), newsCrawler(), notificationCrawler()]);

    const information = {
      bod: results[0],
      structure: results[1],
      workingSchedule: results[2],
      news: results[3],
      notifications: results[4]
    };
    res.send(information).status(200);
  } else {
    res.send([]).status(200);
  }
}

module.exports = crawlDataBinhThuan;