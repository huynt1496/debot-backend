const rp = require("request-promise");
const cheerio = require("cheerio");
const puppeteer = require('puppeteer');

const bodURL = 'https://sgddt.tiengiang.gov.vn/lanh-ao-nganh1';
const structureURL = 'https://sgddt.tiengiang.gov.vn/so-o-to-chuc1';
const wsURL = 'https://sgddt.tiengiang.gov.vn/lich-cong-tac-tuan';
const ntURL = 'https://sgddt.tiengiang.gov.vn/thong-tin-thi';
const newsURL = 'https://sgddt.tiengiang.gov.vn/tin-tuc-su-kien';

const options = async (url) => {
  const browser = await puppeteer.launch();
  const newPage = await browser.newPage();
  await newPage.goto(url);

  const datatotalPage = await newPage.evaluate(() => document.body.outerHTML);
  var $ = cheerio.load(datatotalPage);
  await browser.close();

  return $;
};

const bodCrawler = async () => {
  try {
    var $ = await options(bodURL);
  } catch (error) {
    return error;
  }
  const bodContent = $("div.journal-content-article");
  const bodURL1 = 'src="/documents/';
  const bodURL2 = 'src="https://sgddt.tiengiang.gov.vn/documents/';

  const dataBodContent = {
    data: bodContent.html().replaceAll(bodURL1, bodURL2),
    type: "html",
  };
  return dataBodContent;
};

const wsCrawler = async () => {
  try {
    var $ = await options(wsURL);
  } catch (error) {
    return error;
  }

  const WsContent = $("#portlet_48_INSTANCE_J1ELrqp1BFLr");

  const dataWsContent = {
    data: WsContent.html(),
    type: "html",
  };

  return dataWsContent;
  // try {
  //   var $ = await options(wsURL);
  // } catch (error) {
  //   return error;
  // }
  // const wsContent = $("#lblTitle > table > tbody > tr");
  // var page_ws = {};
  // let data = [];
  // for (let i = 0; i < wsContent.length; i++) {
  //   const title = $(`#lblTitle > table > tbody > tr:nth-child(${i + 1})`).text().trim();
  //   data.push({
  //     title,
  //   });
  // }
  // data.push({
  //   title: 'Xem thêm',
  //   URL_title: wsURL,
  // });

  // page_ws = {
  //   1: data,
  // }

  // return page_ws;
};

const structureCrawler = async () => {
  try {
    var $ = await options(structureURL);
  } catch (error) {
    return error;
  }
  const digContent = $("div.portlet-body > div > p:nth-child(3) > img");

  const dataDigContent = {
    data: 'https://sgddt.tiengiang.gov.vn' + digContent?.attr('src'),
    type: "img",
  };
  return dataDigContent;
};

const notificationCrawler = async () => {
  try {
    var $ = await options(ntURL);
  } catch (error) {
    return error;
  }
  const notificationContent = $("div.portlet-content > div > div > div > div");
  var page_notification = {}
  let data = [];
  for (let i = 0; i < notificationContent.length - 4; i++) {
    const URL = "https://sgddt.tiengiang.gov.vn";
    const data_avatar = $(`div.portlet-content > div > div > div > div:nth-child(${i + 1}) > div > div > a > img`);
    const title = $(`div.portlet-content > div > div > div > div:nth-child(${i + 1}) > span > a`).text().trim();
    const URL_title = URL + $(`div.portlet-content > div > div > div > div:nth-child(${i + 1}) > span > a`).attr('href');
    const date = $(`div.portlet-content > div > div > div > div:nth-child(${i + 1}) > span > span`).text().trim();
    const des = $(`div.portlet-content > div > div > div > div:nth-child(${i + 1}) > p`).text().trim();
    const avatar = data_avatar.attr('src');
    data.push({
      title,
      date,
      URL_title,
      des,
      avatar
    });
  }

  data.push({
    title: 'Xem thêm',
    URL_title: 'https://sgddt.tiengiang.gov.vn/thong-tin-thi',
  });

  page_notification = {
    1: data,
  }

  return page_notification;
};

const newsCrawler = async () => {
  try {
    var $ = await options(newsURL);
  } catch (error) {
    return error;
  }
  const notificationContent = $("div.portlet-content > div > div > div > div");
  var page_news = {}
  let data = [];
  for (let i = 0; i < notificationContent.length - 4; i++) {
    const URL = "https://sgddt.tiengiang.gov.vn";
    const data_avatar = $(`div.portlet-content > div > div > div > div:nth-child(${i + 1}) > div > div > a > img`);
    const title = $(`div.portlet-content > div > div > div > div:nth-child(${i + 1}) > span > a`).text().trim();
    const URL_title = URL + $(`div.portlet-content > div > div > div > div:nth-child(${i + 1}) > span > a`).attr('href');
    const date = $(`div.portlet-content > div > div > div > div:nth-child(${i + 1}) > span > span`).text().trim();
    const des = $(`div.portlet-content > div > div > div > div:nth-child(${i + 1}) > p`).text().trim();
    const avatar = data_avatar.attr('src');
    data.push({
      title,
      date,
      URL_title,
      des,
      avatar
    });
  }

  data.push({
    title: 'Xem thêm',
    URL_title: 'https://sgddt.tiengiang.gov.vn/tin-tuc-su-kien',
  });

  page_news = {
    1: data,
  }

  return page_news;
};

const crawlTienGiang = async (req, res) => {
  const { contentPage, page } = req?.body;
  if (!contentPage) {
    const results = await Promise.all([bodCrawler(), structureCrawler(), wsCrawler(), newsCrawler(), notificationCrawler()]);

    const information = {
      bod: results[0],
      structure: results[1],
      workingSchedule: results[2],
      news: results[3],
      notifications: results[4]
    };
    res.send(information).status(200);
  } else {
    res.send([]).status(200);
  }
}
module.exports = crawlTienGiang;