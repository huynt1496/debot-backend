const rp = require("request-promise");
const cheerio = require("cheerio");

const bodURL = 'http://sgddt.dongnai.gov.vn/gioi-thieu/co-cau-to-chuc/ban-giam-doc.html';
const structureURL = 'http://sgddt.dongnai.gov.vn/gioi-thieu/so-luoc-ve-nganh-gd-dt-dong-nai.html';
const wsURL = 'http://sgddt.dongnai.gov.vn/tien-ich/lich-cong-tac.html';
const ntURL = 'http://sgddt.dongnai.gov.vn/chi-dao/thong-bao.html';
const newsURL = 'http://sgddt.dongnai.gov.vn/tin-tuc.html';


const options = (url, page) => {
  let newUrl = url;
  if (page) {
    newUrl = `${url}?page=${page}`;
  }
  return {
    uri: newUrl,
    transform: (body) => cheerio.load(body)
  }
};

const bodCrawler = async () => {
  try {
    var $ = await rp(options(bodURL));
  } catch (error) {
    return error;
  }
  const tableContent = $("div#dnn_ctr10516_ModuleContent > div.banlanhdao > div.col-lg-3");
  let data = [];
  for (let i = 4; i < tableContent.length; i++) {
    const data_avatar = $(`div#dnn_ctr10516_ModuleContent > div.banlanhdao:nth-child(${i + 1}) > div.col-lg-3.td1 > img`);
    const title = $(`#dnn_ctr10516_ModuleContent > div.banlanhdao:nth-child(${i + 1}) > div.col-lg-3.td2`).text().trim();
    const info = $(`#dnn_ctr10516_ModuleContent > div.banlanhdao:nth-child(${i + 1}) > div.col-lg-6.td3`).text().trim();
    const avatar = "http://sgddt.dongnai.gov.vn" + data_avatar.attr('src');

    data.push({
      avatar,
      title,
      info,
    });
  }
  return data;
};

const wsCrawler = async (page) => {
  try {
    var $ = await rp(options(wsURL, page));
  } catch (error) {
    return error;
  }
  const wsContent = $("#list_news > div.list-news > div.listnews > div.newstop");
  var page_ws = {};
  let data = [];
  for (let i = 0; i < wsContent.length; i++) {
    const day = $(`div#list_news > div.list-news > div.listnews > div.newstop:nth-child(${i + 1}) > div.object > span.day`).text().trim();
    const date = $(`div#list_news > div.list-news > div.listnews > div.newstop:nth-child(${i + 1}) > div.object > span.date`).text().trim();
    const title = $(`div#list_news > div.list-news > div.listnews > div.newstop:nth-child(${i + 1}) > h3.thumoi`).text().trim();
    const URL_title = $(`div#list_news > div.list-news > div.listnews > div.newstop:nth-child(${i + 1}) > h3.thumoi > a`).attr('href');

    data.push({
      day,
      date,
      title,
      URL_title,
    });
  }
  if (page == '1') {
    const valueMaxPage = Number(13) || 1;
    page_ws = {
      [page]: data,
      totalPage: valueMaxPage
    }
  }
  else {
    page_ws = {
      [page]: data,
    }
  }
  return page_ws;
};

const structureCrawler = async () => {
  try {
    var $ = await rp(options(structureURL));
  } catch (error) {
    return error;
  }
  const digContent = $("#dnn_ctr10494_ModuleContent ");

  const dataDigContent = {
    data: digContent.html(),
    type: "html",
  };
  return dataDigContent;
};

const notificationCrawler = async (page) => {
  try {
    var $ = await rp(options(ntURL, page));
  } catch (error) {
    return error;
  }
  const notificationContent = $("div#list_news > div.list-news > div.listnews > div.newstop");
  var page_notification = {}
  let data = [];
  for (let i = 0; i < notificationContent.length; i++) {
    const day = $(`div#list_news > div.list-news > div.listnews > div.newstop:nth-child(${i + 1})> div > span.day`).text().trim();
    const date = $(`div#list_news > div.list-news > div.listnews > div.newstop:nth-child(${i + 1}) > div > span.date`).text().trim();
    const title = $(`div#list_news > div.list-news > div.listnews > div.newstop:nth-child(${i + 1}) > h3`).text().trim();
    const URL_title = $(`div#list_news > div.list-news > div.listnews > div.newstop:nth-child(${i + 1}) > h3 > a`).attr('href');

    data.push({
      day,
      date,
      title,
      URL_title,
    });
  }
  if (page == '1') {
    const valueMaxPage = Number(10) || 1;
    page_notification = {
      [page]: data,
      totalPage: valueMaxPage
    }
  }
  else {
    page_notification = {
      [page]: data,
    }
  }
  return page_notification;
};

const newsCrawler = async (page) => {
  try {
    var $ = await rp(options(newsURL, page));
  } catch (error) {
    return error;
  }

  const newsContent = $("#list_news > div.list-news > div.listnews > div.newstop ");
  var page_news = {}
  let data = [];
  for (let i = 0; i < newsContent.length; i++) {
    const data_avatar = $(`div#list_news > div.list-news > div.listnews > div.newstop:nth-child(${i + 1}) > div.newstopimg > a > img`);
    const title = $(`div#list_news > div.list-news > div.listnews > div.newstop:nth-child(${i + 1}) > h3.post-title`).text().trim();
    const URL_title = $(`div#list_news > div.list-news > div.listnews > div.newstop:nth-child(${i + 1}) > h3.post-title > a`).attr('href');
    const description = $(`div#list_news > div.list-news > div.listnews > div.newstop:nth-child(${i + 1}) > p.post-sapo`).text();
    const more = $(`div#list_news > div.list-news > div.listnews > div.newstop:nth-child(${i + 1}) > p.post-sapo > a > strong `).text();
    const avatar = data_avatar.attr('src');
    const des = description.replace("Xem thêm", "");
    data.push({
      avatar,
      title,
      URL_title,
      des,
      more,
    });
  }

  if (page == '1') {
    const valueMaxPage = Number(25) || 1;
    page_news = {
      [page]: data,
      totalPage: valueMaxPage
    }

  }
  else {
    page_news = {
      [page]: data,
    }
  }
  return page_news;
};

const crawlDataDongNai = async (req, res) => {
  const { contentPage, page } = req?.body;
  if (!contentPage) {
    const results = await Promise.all([bodCrawler(), structureCrawler(), wsCrawler(1), newsCrawler(1), notificationCrawler(1)]);

    const information = {
      bod: results[0],
      structure: results[1],
      workingSchedule: results[2],
      news: results[3],
      notifications: results[4]
    };
    res.send(information).status(200);
  }
  else {
    if (contentPage == 'ws') {
      const workingScheduleData = await wsCrawler(page);
      const information = {
        workingSchedule: workingScheduleData,
      };
      res.send(information).status(200);
    }
    if (contentPage == 'news') {
      const newsData = await newsCrawler(page);

      const information = {
        news: newsData,
      };
      res.send(information).status(200);
    }
    if (contentPage == 'nt') {
      const notificationData = await notificationCrawler(page);

      const information = {
        notifications: notificationData,
      };
      res.send(information).status(200);
    }
  }
}

module.exports = crawlDataDongNai;