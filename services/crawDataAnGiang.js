const rp = require("request-promise");
const cheerio = require("cheerio");
const puppeteer = require('puppeteer');


const bodURL = 'https://angiang.edu.vn/gioi-thieu/lanh-dao-so/ban-giam-doc';
const structureURL = 'https://angiang.edu.vn/gioi-thieu/so-do-to-chuc';
const wsURL = 'https://angiang.edu.vn/tiep-cong-dan/lich-lam-viec-lanh-dao';
const ntURL = 'https://angiang.edu.vn/tin-tuc-su-kien/thong-bao';
const newsURL = 'https://angiang.edu.vn/tin-tuc-su-kien#module16';

const options = async (url) => {
  const browser = await puppeteer.launch();
  const newPage = await browser.newPage();
  await newPage.goto(url);
  const datatotalPage = await newPage.evaluate(() => document.body.outerHTML);
  var $ = cheerio.load(datatotalPage);
  await browser.close();

  return $;

};

const bodCrawler = async () => {
  try {
    var $ = await options(bodURL);
  } catch (error) {
    return error;
  }
  const tableContent = $("#section-person13 >div");
  let data = [];
  for (let i = 0; i < tableContent.length - 1; i++) {
    const col2 = $(`#section-person13 >div:nth-child(${i + 1}) > ul > li > div >div>figure>img`);

    const title = $(`#section-person13 >div:nth-child(${i + 1}) > ul>li>div.row>div:nth-child(2)>ul>li:nth-child(2)>div:nth-child(2)`).text().trim() + ":       " + $(`#section-person13 >div:nth-child(${i + 1}) > ul>li>div.row>div:nth-child(2)>ul>li:nth-child(1)>div>h2>a`).text().trim().toLocaleUpperCase() + "                                            " + $(`#section-person13 >div:nth-child(${i + 1}) > ul>li>div.row>div:nth-child(2)>ul>li:nth-child(3)`).text().trim();
    const info = $(`#section-person13 >div:nth-child(${i + 1}) > ul>li>div.row>div:nth-child(2)>ul >li:last-child`).text().trim().replace(/-/g, "");
    const avatar = "https://angiang.edu.vn/" + col2.attr('src');

    data.push({
      avatar,
      info,
      title
    });
  }
  return data;
};
const wsCrawler = async () => {
  try {
    var $ = await options(wsURL);
  } catch (error) {
    return error;
  }
  const wsContent = $("#section16 > article");
  var page_news = {};
  let data = [];
  for (let i = 0; i < wsContent.length; i++) {
    const data_avatar = $(`#section16 > article:nth-child(${i + 1}) > div > div > figure > a > img`);
    const title = $(`#section16 > article:nth-child(${i + 1}) > div > div > div > h4 >a`).text().trim();
    const URL_title = "https://angiang.edu.vn/" + $(`#section16 > article:nth-child(${i + 1}) >div >div:nth-child(2)>div>h4>a`).attr('href');

    const date = $(`#section16 > article:nth-child(${i + 1}) > div > div > div:nth-child(2)>span:nth-child(2)`).text().trim();
    const seen = $(`#section16 > article:nth-child(${i + 1}) > div > div > div:nth-child(2)>span:nth-child(4)`).text().trim();
    // const des = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1}) > div > div.cat-des`).text() || "[...]";
    const avatar = "https://angiang.edu.vn/" + data_avatar.attr('src');
    data.push({
      avatar,
      title,
      URL_title,
      // user,
      date,
      seen,
      // des
    });
  }

  data.push({
    title: 'Xem thêm',
    URL_title: wsURL,
  });

  page_ws = {
    1: data,
  }

  return page_ws;
};

const structureCrawler = async () => {
  try {
    var $ = await options(structureURL);
  } catch (error) {
    return error;
  }
  const digContent = $("#module16.ModuleWrapper  > section >article >div>p >img");

  const dataDigContent = {
    data: "https://angiang.edu.vn/" + digContent?.attr('src'),
    type: "img",
  };
  return dataDigContent;
};
const notificationCrawler = async () => {
  try {
    var $ = await options(ntURL);
  } catch (error) {
    return error;
  }
  const notificationContent = $("#section16 > article");
  var page_news = {}
  let data = [];
  for (let i = 0; i < notificationContent.length; i++) {
    const data_avatar = $(`#section16 > article:nth-child(${i + 1})>div>div>figure>a>img`);
    const title = $(`#section16 > article:nth-child(${i + 1})>div>div:nth-child(2)>div>h4>a`).text().trim();
    const URL_title = "https://angiang.edu.vn/" + $(`#section16 > article:nth-child(${i + 1})>div>div:nth-child(2)>div>h4>a`).attr('href');

    const date = $(`#section16 > article:nth-child(${i + 1})>div>div:nth-child(2)>div:nth-child(2)>span:nth-child(2)`).text().trim();
    const seen = $(`#section16 > article:nth-child(${i + 1})>div>div:nth-child(2)>div:nth-child(2)>span:nth-child(4)`).text().trim();
    const des = $(`#section16 > article:nth-child(${i + 1})>div>div:nth-child(2)>div:nth-child(3)`).text() || "[...]";
    const avatar = "https://angiang.edu.vn/" + data_avatar.attr('src');

    data.push({
      avatar,
      title,
      URL_title,
      seen,
      des,
      date,

    });
  }
  data.push({
    title: 'Xem thêm',
    URL_title: ntURL,
  });

  page_notification = {
    1: data,
  }

  return page_notification;
};

const newsCrawler = async () => {
  try {
    var $ = await options(newsURL);
  } catch (error) {
    return error;
  }
  const newsContent = $("div#module16>section>article");
  var page_news = {}
  let data = [];
  for (let i = 0; i < newsContent.length; i++) {
    const data_avatar = $(`#module16>section>article:nth-child(${i + 1})>div>div>figure>a>img`);
    const title = $(`#module16>section>article:nth-child(${i + 1})>div>div:nth-child(2)>div>h4>a`).text().trim();
    const URL_title = "https://angiang.edu.vn/" + $(`#module16>section>article:nth-child(${i + 1})>div>div:nth-child(2)>div>h4>a`).attr('href');
    const date = $(`#module16>section>article:nth-child(${i + 1})>div>div:nth-child(2)>div:nth-child(2)>span:nth-child(2)`).text().trim();
    const seen = $(`#module16>section>article:nth-child(${i + 1})>div>div:nth-child(2)>div:nth-child(2)>span:nth-child(4)`).text().trim();
    const des = $(`#module16>section>article:nth-child(${i + 1})>div>div:nth-child(2)>div:nth-child(3)`).text() || "[...]";
    const avatar = "https://angiang.edu.vn/" + data_avatar.attr('src');
    data.push({
      avatar,
      title,
      URL_title,
      // user,
      date,
      seen,
      des
    });
  }

  data.push({
    title: 'Xem thêm',
    URL_title: newsURL,
  });

  page_news = {
    1: data,
  }

  return page_news;
};

const crawlDataAnGiang = async (req, res) => {
  const { contentPage, page } = req?.body;
  if (!contentPage) {
    const results = await Promise.all([bodCrawler(), structureCrawler(), wsCrawler(), newsCrawler(), notificationCrawler()]);

    const information = {
      bod: results[0],
      structure: results[1],
      workingSchedule: results[2],
      news: results[3],
      notifications: results[4]
    };
    res.send(information).status(200);
  } else {
    res.send([]).status(200);
  }
}
module.exports = crawlDataAnGiang;
