const rp = require("request-promise");
const cheerio = require("cheerio");
const puppeteer = require('puppeteer');

const URL = 'https://khanhhoa.edu.vn/';
const bodURL = 'https://khanhhoa.edu.vn/co-cau-to-chuc/ban-giam-doc';
// const structureURL = 'https://khanhhoa.edu.vn/gioi-thieu/so-do-to-chuc';
const wsURL = 'https://khanhhoa.edu.vn/tin-tuc-su-kien/lich-cong-tac';
const ntURL = 'https://khanhhoa.edu.vn/tin-tuc-su-kien/thong-bao';
const newsURL = 'https://khanhhoa.edu.vn/tin-tuc-su-kien';
let wsTotalPage = 1;
let ntTotalPage = 1;;
let newsTotalPage = 1;

const options = async (url, page, totalNumPage) => {

  let newUrl = url;
  const browser = await puppeteer.launch({ headless: true });
  const new_page = await browser.newPage();

  await new_page.setViewport({
    width: 1366,
    height: 768
  });

  await new_page.goto(newUrl, { waitUntil: 'networkidle0' });

  if (page) {
    if (page < 6) {
      const elNext = await new_page.waitForXPath(`//*[@id="pagination10"]/a[text() = "${page}"]`);
      await elNext?.click();
      await new_page.waitForTimeout(1000);
    }
    else
      if (totalNumPage) {

        if (totalNumPage == page) {
          const elNext = await new_page.waitForXPath(`//*[@id="pagination10"]/a[@class = "last"]`);
          await elNext?.click();
          await new_page.waitForTimeout(1000);
        }
        else
          if (page <= totalNumPage / 2) {

            for (let i = 5; i <= page; i += 2) {
              const elNext = await new_page.waitForXPath(`//*[@id="pagination10"]/a[text() = "${i}"]`);
              await elNext?.click();
              await new_page.waitForTimeout(1000);
            }

            if (page % 2 == 0) {
              const elNext = await new_page.waitForXPath(`//*[@id="pagination10"]/a[text() = "${page}"]`);
              await elNext?.click();
            }

            await new_page.waitForTimeout(1000);
          }
          else {

            const elNext = await new_page.waitForXPath(`//*[@id="pagination10"]/a[@class = "last"]`);
            await elNext?.click();
            await new_page.waitForTimeout(1000);
            let index = 0;

            for (index = totalNumPage - 3; index >= page; index -= 3) {
              const elNext = await new_page.waitForXPath(`//*[@id="pagination10"]/a[text() = "${index}"]`);
              await elNext?.click();
              await new_page.waitForTimeout(1000);
            }

            if (page != index + 3) {
              const elNext = await new_page.waitForXPath(`//*[@id="pagination10"]/a[text() = "${page}"]`);
              await elNext?.click();
            }

            await new_page.waitForTimeout(1000);
          }
      }
  }
  const datatotalPage = await new_page.evaluate(() => document.body.outerHTML);
  var $ = cheerio.load(datatotalPage);
  await browser.close();

  return $;

};



const bodCrawler = async () => {
  try {
    var $ = await options(bodURL);
  } catch (error) {
    return error;
  }

  const bodContent = $("#section-person10 > div.person-type1.person-list-table-type1");
  let data = [];
  for (let i = 0; i < bodContent.length; i++) {

    const itemInfo = $(`#section-person10 > div:nth-child(1) > ul > li > div > div.col-xs-12.col-sm-10.col-md-10 > ul > li`)
    let info = "\n";
    for (let j = 0; j < itemInfo.length; j++) {
      info += $(`#section-person10 > div:nth-child(1) > ul > li > div > div.col-xs-12.col-sm-10.col-md-10 > ul > li:nth-child(${j + 1})`).text() + "\n"
    }
    const bod_avatar = $(`#section-person10 > div:nth-child(${i + 1}) > ul > li > div > div.col-xs-12.col-sm-2.col-md-2 > figure > img`);
    const avatar = `${URL}${bod_avatar.attr('src')}`;

    data.push({
      avatar,
      info,
    });
  }
  return data;
};

const structureCrawler = async () => {

  const dataDigContent = {
    data: `<h3>Không có dữ liệu</h3>`,
    type: "html",
  };
  return dataDigContent;
};

const setTotalPage = (url, page) => {

  switch (url) {
    case ntURL:
      ntTotalPage = page;
      break;

    case wsURL:
      wsTotalPage = page;
      break;

    case newsURL:
      newsTotalPage = page;
      break;

    default:
      break;
  }
}

const Crawler = async (url, page, totalNumPage) => {
  try {
    if (page > 1)
      var $ = await options(url, page, totalNumPage);
    else
      var $ = await options(url);
  } catch (error) {
    return error;
  }

  const Content = $("#section10 > article");
  var page_news = {};
  let data = [];
  for (let i = 0; i < Content.length; i++) {
    const data_avatar = $(`#section10 > article:nth-child(${i + 1}) > div > div.col-xs-12.col-sm-4.col-md-4 > figure > a > img`);
    const title = $(`#section10 > article:nth-child(${i + 1}) > div > div.col-xs-12.col-sm-8.col-md-8.right-detailType2 > div.post-title > h4 > a`).text().trim();
    let URL_title = $(`#section10 > article:nth-child(${i + 1}) > div > div.col-xs-12.col-sm-8.col-md-8.right-detailType2 > div.post-title > h4 > a`).attr('href');
    const valueDate = $(`#section10 > article:nth-child(${i + 1}) > div > div.col-xs-12.col-sm-8.col-md-8.right-detailType2 > div.time-news > span.time-v`).text().trim();
    const seen = $(`#section10 > article:nth-child(${i + 1}) > div > div.col-xs-12.col-sm-8.col-md-8.right-detailType2 > div.time-news > span.luotxem`).text().trim();
    const des = $(`#section10 > article:nth-child(${i + 1}) > div > div.col-xs-12.col-sm-8.col-md-8.right-detailType2 > div.brief-news`).text() || "[...]";
    const avatar = `${URL}${data_avatar.attr('src')}` || "";
    const date = valueDate.replace(/[Ngày đăng :()]/g, '') || "";
    URL_title = `${URL}${URL_title}` || "";
    data.push({
      avatar,
      title,
      URL_title,
      date,
      seen,
      des
    });
  }

  if (page == 1) {
    let indexTotal = $.html().indexOf("totalItems");
    let valueTotal = $.html().slice(indexTotal, indexTotal + 18).replace(/[totalItems:, orderBy]/g, '')
    const valueMaxPage = Math.floor(Number(valueTotal / 12) + 1) || 1;
    setTotalPage(url, valueMaxPage);
    page_news = {
      [page]: data,
      totalPage: valueMaxPage
    }
  }
  else {
    page_news = {
      [page]: data,
    }
  }
  return page_news;
};

const crawlKhanhHoa = async (req, res) => {

  const { contentPage, page } = req?.body;

  if (!contentPage) {

    const bodData = await bodCrawler();
    const structureData = await structureCrawler();
    const workingScheduleData = await Crawler(wsURL, 1);
    const newsData = await Crawler(newsURL, 1);
    const notificationData = await Crawler(ntURL, 1);

    const information = {
      bod: bodData,
      structure: structureData,
      workingSchedule: workingScheduleData,
      news: newsData,
      notifications: notificationData
    };

    res.send(information).status(200);
  }
  else {

    if (contentPage == 'ws') {
      const workingScheduleData = await Crawler(wsURL, page, wsTotalPage);
      const information = {
        workingSchedule: workingScheduleData,
      };
      res.send(information).status(200);
    }

    if (contentPage == 'news') {
      const newsData = await Crawler(newsURL, page, newsTotalPage);

      const information = {
        news: newsData,
      };
      res.send(information).status(200);
    }

    if (contentPage == 'nt') {
      const notificationData = await Crawler(ntURL, page, ntTotalPage);
      const information = {
        notifications: notificationData,
      };
      res.send(information).status(200);
    }
  }
}

module.exports = crawlKhanhHoa;