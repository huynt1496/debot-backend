const rp = require("request-promise");
const cheerio = require("cheerio");

const bodURL = 'http://daknong.edu.vn/phong-ban/ban-giam-doc/';
const structureURL = 'http://daknong.edu.vn/so-do-to-chuc/';
const wsURL = 'http://daknong.edu.vn/chuyen-muc/tiep-cong-dan/lich-tiep-cong-dan/';
const ntURL = 'http://daknong.edu.vn/chuyen-muc/thong-bao/';
const newsURL = 'http://daknong.edu.vn/chuyen-muc/tin-tuc-su-kien';


const options = (url, page) => {
  let newUrl = url;
  if (page) {
    newUrl = `${url}/page/${page}`;
  }
  return {
    uri: newUrl,
    transform: (body) => cheerio.load(body)
  }
};

const bodCrawler = async () => {
  try {
    var $ = await rp(options(bodURL));
  } catch (error) {
    return error;
  }
  const bodContent = $("#main-content > div > div.s-content > div > div.thongtin-canbo");
  let data = [];
  for (let i = 1; i < bodContent.length + 1; i++) {
    const info = $(`#main-content > div > div.s-content > div > div.thongtin-canbo:nth-child(${i + 1}) > div.thongtin-canbo-caption > span`).text().trim();
    const bod_avatar = $(`#main-content > div > div.s-content > div > div.thongtin-canbo:nth-child(${i + 1}) > div.thongtin-canbo-img > img`);
    const avatar = bod_avatar.attr('src');

    data.push({
      avatar,
      info,
    });
  }
  return data;
};

const wsCrawler = async (page) => {
  try {
    var $ = await rp(options(wsURL, page));
  } catch (error) {
    return error;
  }
  const wsContent = $("#main-content.main-content > div > div > div.cat-content > div.cat-item");
  var page_ws = {};
  let data = [];
  for (let i = 0; i < wsContent.length; i++) {
    const data_avatar = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1}) > div > a > img`);
    const title = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1}) > div.cat-caption > h2.cat-title`).text().trim();
    const URL_title = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1}) > div.cat-caption > h2.cat-title > a`).attr('href');
    const user = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1}) > div.cat-caption > div > p > span:nth-child(1)`).text().trim();
    const date = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1}) > div.cat-caption > div > p > span:nth-child(2)`).text().trim();
    const seen = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1}) > div.cat-caption > div > p > span:nth-child(3)`).text().trim();
    const des = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1}) > div > div.cat-des`).text() || "[...]";
    const avatar = data_avatar.attr('src');
    data.push({
      avatar,
      title,
      URL_title,
      user,
      date,
      seen,
      des
    });
  }
  if (page == '1') {
    const stringPage = $("#main-content.main-content > div > div > div.ddev-pagenavi > span.pages").text().trim();
    const index = stringPage.lastIndexOf('/ ');
    const valueMaxPage = Number(stringPage.slice(index + 1)) || 1;
    page_ws = {
      [page]: data,
      totalPage: valueMaxPage
    }
  }
  else {
    page_ws = {
      [page]: data,
    }
  }
  return page_ws;
};

const structureCrawler = async () => {
  try {
    var $ = await rp(options(structureURL));
  } catch (error) {
    return error;
  }
  const digContent = $("#main-content > div > div > div > div > p > img");
  const dataDigContent = {
    data: digContent?.attr('src'),
    type: "img",
  };
  return dataDigContent;
};

const notificationCrawler = async (page) => {
  try {
    var $ = await rp(options(ntURL, page));
  } catch (error) {
    return error;
  }
  const notificationContent = $("#main-content.main-content > div > div > div.cat-content > div.cat-item");
  var page_notification = {}
  let data = [];
  for (let i = 0; i < notificationContent.length; i++) {
    const data_avatar = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1}) > div > a > img`);
    const title = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1}) > div.cat-caption > h2.cat-title`).text().trim();
    const URL_title = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1}) > div.cat-caption > h2.cat-title > a`).attr('href');
    const user = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1})  > div.cat-caption > div > p:nth-child(1) > span:nth-child(1)`).text().trim();
    const date = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1}) > div.cat-caption > div > p > span:nth-child(2)`).text().trim();
    const seen = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1})  > div.cat-caption > div > p:nth-child(2) > span:nth-child(1)`).text().trim();
    const avatar = data_avatar.attr('src');
    data.push({
      avatar,
      title,
      URL_title,
      user,
      date,
      seen,
    });
  }
  if (page == '1') {
    const stringPage = $("#main-content.main-content > div > div > div.ddev-pagenavi > span.pages").text().trim();
    const index = stringPage.lastIndexOf('/ ');
    const valueMaxPage = Number(stringPage.slice(index + 1)) || 1;
    page_notification = {
      [page]: data,
      totalPage: valueMaxPage
    }
  }
  else {
    page_notification = {
      [page]: data,
    }
  }
  return page_notification;
};

const newsCrawler = async (page) => {
  try {
    var $ = await rp(options(newsURL, page));
  } catch (error) {
    return error;
  }
  const newsContent = $("#main-content.main-content > div > div > div.cat-content > div.cat-item");
  var page_news = {}
  let data = [];
  for (let i = 0; i < newsContent.length; i++) {
    const data_avatar = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1}) > div > a > img`);
    const title = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1}) > div.cat-caption > h2.cat-title`).text().trim();
    const URL_title = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1}) > div.cat-caption > h2.cat-title > a`).attr('href');
    const user = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1})  > div.cat-caption > div > p:nth-child(1) > span:nth-child(1)`).text().trim();
    const date = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1}) > div.cat-caption > div > p > span:nth-child(2)`).text().trim();
    const seen = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1})  > div.cat-caption > div > p:nth-child(1) > span:nth-child(3)`).text().trim();
    const des = $(`div#main-content.main-content > div > div > div.cat-content > div.cat-item:nth-child(${i + 1}) > div > div.cat-des`).text() || "[...]";
    const avatar = data_avatar.attr('src');
    data.push({
      avatar,
      title,
      URL_title,
      user,
      date,
      seen,
      des
    });
  }

  if (page == '1') {
    const stringPage = $("#main-content.main-content > div > div > div.ddev-pagenavi > span.pages").text().trim();
    const index = stringPage.lastIndexOf('/ ');
    const valueMaxPage = Number(stringPage.slice(index + 1)) || 1;
    page_news = {
      [page]: data,
      totalPage: valueMaxPage
    }
  }
  else {
    page_news = {
      [page]: data,
    }
  }


  return page_news;
};

const crawlDakNong = async (req, res) => {
  const { contentPage, page } = req?.body;
  if (!contentPage) {
    const results = await Promise.all([bodCrawler(), structureCrawler(), wsCrawler(1), newsCrawler(1), notificationCrawler(1)]);

    const information = {
      bod: results[0],
      structure: results[1],
      workingSchedule: results[2],
      news: results[3],
      notifications: results[4]
    };
    res.send(information).status(200);
  }
  else {
    if (contentPage == 'ws') {
      const workingScheduleData = await wsCrawler(page);
      const information = {
        workingSchedule: workingScheduleData,
      };
      res.send(information).status(200);
    }
    if (contentPage == 'news') {
      const newsData = await newsCrawler(page);

      const information = {
        news: newsData,
      };
      res.send(information).status(200);
    }
    if (contentPage == 'nt') {
      const notificationData = await notificationCrawler(page);

      const information = {
        notifications: notificationData,
      };
      res.send(information).status(200);
    }
  }
}

module.exports = crawlDakNong;