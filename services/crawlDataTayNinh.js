const rp = require("request-promise");
const cheerio = require("cheerio");
const puppeteer = require('puppeteer');

const URL = 'http://tayninh.edu.vn/';
const bodURL = 'http://tayninh.edu.vn/sogdtnh/4/469/2252/3698/Co-cau-to-chuc/Co-cau-to-chuc-So-Giao-duc-va-Dao-tao-Tay-Ninh.aspx';
const structureURL = 'http://tayninh.edu.vn/sogdtnh/4/469/2252/111987/Co-cau-to-chuc/Thong-tin-cac-don-vi-truc-thuoc-So-giao-duc-va-Dao-tao.aspx';
const wsURL = 'http://tayninh.edu.vn/sogdtnh/4/467/2258/Lich-cong-tac/';
const ntURL = 'http://tayninh.edu.vn/sogdtnh/4/467/51267/Ke-hoach-phat-trien-nganh-dia-phuong/';
const newsURL = 'http://tayninh.edu.vn/sogdtnh/4/467/2229/Tin-tuc/';

const options = async (url) => {
  const browser = await puppeteer.launch();
  const newPage = await browser.newPage();

  await newPage.goto(url);

  const datatotalPage = await newPage.evaluate(() => document.body.outerHTML);
  var $ = cheerio.load(datatotalPage);

  await browser.close();

  return $;
};

const bodCrawler = async () => {
  try {
    var $ = await options(bodURL);
  } catch (error) {
    return error;
  }
  const digContent = $("div.ArticleDetailControl ");
  const dataURL = 'src="/SiteFolders/Root/';
  const dataURLNew = 'src="http://tayninh.edu.vn/SiteFolders/Root/';
  const dataDigContent = {
    data: digContent.html().replaceAll(dataURL, dataURLNew),
    type: "html",
  };
  return dataDigContent;
};

const wsCrawler = async () => {
  try {
    var $ = await options(wsURL);
  } catch (error) {
    return error;
  }

  const wsContent = $("#ctrl_6795_30_lvwArticleInMenu_itemPlaceholderContainer > li");
  var page_ws = {};
  let data = [];
  for (let i = 0; i < wsContent.length; i++) {
    const title = $(`#ctrl_6795_30_lvwArticleInMenu_itemPlaceholderContainer > li:nth-child(${i + 1}) h2 > a`).text().trim();
    let URL_title = URL + $(`#ctrl_6795_30_lvwArticleInMenu_itemPlaceholderContainer > li:nth-child(${i + 1}) > h2 > a`).attr('href');
    data.push({
      title,
      URL_title,
    });
  }

  data.push({
    title: 'Xem thêm',
    URL_title: wsURL,
  });

  page_ws = {
    1: data,
  }
  return page_ws;
};

const structureCrawler = async () => {
  try {
    var $ = await options(structureURL);
  } catch (error) {
    return error;
  }
  const digContent = $("div.ArticleDetailControl ");

  const dataDigContent = {
    data: digContent.html(),
    type: "html",
  };
  return dataDigContent;
};

const notificationCrawler = async () => {
  try {
    var $ = await options(ntURL);
  } catch (error) {
    return error;
  }

  const notificationContent = $("#ctrl_6795_30_lvwArticleInMenu_itemPlaceholderContainer > li");
  var page_notification = {};
  let data = [];
  for (let i = 0; i < notificationContent.length; i++) {
    const data_avatar = $(`#ctrl_6795_30_lvwArticleInMenu_itemPlaceholderContainer > li:nth-child(${i + 1}) > a > img`);
    const title = $(`#ctrl_6795_30_lvwArticleInMenu_itemPlaceholderContainer > li:nth-child(${i + 1}) h2 > a`).text().trim();
    let URL_title = URL + $(`#ctrl_6795_30_lvwArticleInMenu_itemPlaceholderContainer > li:nth-child(${i + 1}) > h2 > a`).attr('href');
    const avatar = `${URL}${data_avatar.attr('src')}`;
    data.push({
      avatar,
      title,
      URL_title,
    });
  }
  data.push({
    title: 'Xem thêm',
    URL_title: ntURL,
  });

  page_notification = {
    1: data,
  }
  return page_notification;
};

const newsCrawler = async () => {
  try {
    var $ = await options(newsURL);
  } catch (error) {
    return error;
  }

  const newsContent = $("#ctrl_6795_30_lvwArticleInMenu_itemPlaceholderContainer > li");
  var page_news = {};
  let data = [];
  for (let i = 0; i < newsContent.length; i++) {
    const data_avatar = $(`#ctrl_6795_30_lvwArticleInMenu_itemPlaceholderContainer > li:nth-child(${i + 1}) > a > img`);
    const title = $(`#ctrl_6795_30_lvwArticleInMenu_itemPlaceholderContainer > li:nth-child(${i + 1}) h2 > a`).text().trim();
    let URL_title = URL + $(`#ctrl_6795_30_lvwArticleInMenu_itemPlaceholderContainer > li:nth-child(${i + 1}) > h2 > a`).attr('href');
    const avatar = `${URL}${data_avatar.attr('src')}`;
    data.push({
      avatar,
      title,
      URL_title,
    });
  }
  data.push({
    title: 'Xem thêm',
    URL_title: newsURL,
  });
  page_news = {
    1: data,
  }

  return page_news;
};

const crawlDataTayNinh = async (req, res) => {
  const { contentPage, page } = req?.body;
  if (!contentPage) {
    const results = await Promise.all([bodCrawler(), structureCrawler(), wsCrawler(), newsCrawler(), notificationCrawler()]);

    const information = {
      bod: results[0],
      structure: results[1],
      workingSchedule: results[2],
      news: results[3],
      notifications: results[4]
    };
    res.send(information).status(200);
  } else {
    res.send([]).status(200);
  }
}

module.exports = crawlDataTayNinh;