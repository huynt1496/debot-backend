const rp = require("request-promise");
const cheerio = require("cheerio");
const axios = require("axios");
const moment = require("moment");

const bodURL = "https://lamdong.edu.vn/vi/sgddetail/?param=sgd_mc_bod";
const structureURL = "https://lamdong.edu.vn/vi/sgddetail/?param=sgd_mc_departments";
const wsURL = "https://lamdong.edu.vn/api/Management/LichHopCongTacPublicNew/GetSchedule";
const ntCode = "sgd_notification";
const newsCode = "sgd_news";
const api_URL = "https://lamdong.edu.vn/Modules/NewsHome/GetNewsList2";

const options = (url, page) => {
  let newUrl = url;
  if (page) {
    newUrl = `${url}/page/${page}`;
  }
  return {
    uri: newUrl,
    transform: (body) => cheerio.load(body),
  };
};
const bodCrawler = async () => {
  try {
    var $ = await rp(options(bodURL));
  } catch (error) {
    return error;
  }
  const tableContent = $("div.content-post > div > table > tbody > tr");
  let data = [];
  for (let i = 0; i < tableContent.length; i++) {
    const col2 = $(
      `div.content-post > div > table > tbody > tr:nth-child(${i + 1}) > td > img`
    );
    const col3 = $(
      `div.content-post > div > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(2)`
    );
    const info = col3.text();
    const avatar = col2.attr("src");
    data.push({
      avatar,
      info,
    });
  }
  return data;
};

convertNumToDay = (day) => {
  switch (day) {
    case 0:
      return "Chủ nhật";
    case 1:
      return "Thứ hai";
    case 2:
      return "Thứ ba";
    case 3:
      return "Thứ tư";
    case 4:
      return "Thứ năm";
    case 5:
      return "Thứ sáu";
    case 6:
      return "Thứ bảy";
    default:
      return "0";
  }
};

const wsCrawler = async (page) => {
  let year, week;
  if (page == 1) {
    const date = new Date();
    week = Number(moment(date).format("W")) + 1;
    year = date.getFullYear();
    page = `${year}${week}`;
  } else {
    year = page?.slice(0, 4) || 0;
    week = page?.slice(4) || 0;
  }
  try {
    const data = await axios
      .post(wsURL, { Week: week, Year: year, Type: "tong-hop" })
      .then((res) => res.data)
      .catch((res) => {
        console.log(res);
      });
    const content = data.Result.Content;
    let dateOfWeeks = [];
    data.Result.DateOfWeeks.map((value) => {
      const date = value.DateString;
      const day = convertNumToDay(value.DayOfWeek);
      let content = "";
      let combination = "";
      let assignment = "";
      value.Details.map((ws) => {
        content += ws?.Content.replace(/\n|\r/, "") + "\n" || "";
        assignment += ws?.User.replace(/\n|\r/, "") + "\n" || "";
        combination += ws?.Partner.replace(/\n|\r/, "") + "\n" || "";
      });
      dateOfWeeks.push({
        day,
        date,
        content,
        assignment,
        combination,
      });
    });
    const data_ws = {
      content,
      dateOfWeeks,
    };
    const wsDataCrawler = {
      [page]: [data_ws],
      type: "table",
    };
    return wsDataCrawler;
  } catch (error) {
    return error;
  }
};

const structureCrawler = async () => {
  try {
    var $ = await rp(options(structureURL));
  } catch (error) {
    return error;
  }
  const digContent = $("div.cls-content-item");

  const dataDigContent = {
    data: digContent.html(),
    type: "html",
  };
  return dataDigContent;
};

const CrawlerApi = async (Code, page) => {
  try {
    var page_Crawler = {};
    const resData = await axios
      .post(api_URL, {
        page: page,
        pageSize: 15,
        keyword: "",
        Code: Code,
        number: 100000,
      })
      .then((res) => res?.data)
      .catch((res) => {
        console.log(res);
      });
    let data = [];
    let TOTALPAGE = 0;
    resData?.map(async (value) => {
      const avatar = `https://edumedia.dalat.vn/${value?.URL_IMG}`;
      const title = value?.TITLE;
      const date = value?.CREATE;
      const URL_title = `https://lamdong.edu.vn/vi/sgddetail/?param=0${value?.ALIAS}`;
      let des = value.SHORT_CONTENT;
      TOTALPAGE = value.TOTALPAGE;

      if (des == null) {
        const $ = cheerio.load(value?.CONTENT);
        des = $.text()
          .trim()
          .slice(0, 250)
          .replace(/[\r\n]/g, "").replace(/\s+/g, ' ');
        let indexLastSpace = des?.lastIndexOf(" ");
        if (indexLastSpace > 245) des = des?.slice(0, indexLastSpace);
      }
      data.push({
        avatar,
        title,
        URL_title,
        date,
        des,
      });
    });
    const valueMaxPage = Math.floor(TOTALPAGE / 15 + 1);
    page_Crawler = {
      [page]: data,
      totalPage: valueMaxPage,
    };
    return page_Crawler;
  } catch (error) {
    return error;
  }
};

// const Crawler = async (URL, page ) => { // Lấy dữ liệu bằng giao diện
//   try {
//       const browser = await puppeteer.launch({ headless: true});
//       const new_page = await browser.newPage();

//       await new_page.setViewport({
//         width: 1366,
//         height: 768
//      });

//       await new_page.goto(URL, { waitUntil: 'networkidle0' });

//       if(page == '1')
//       {
//         let dataContent = await new_page.evaluate(() => document.body.outerHTML);
//         var $ = cheerio.load(dataContent);
//         const elNext = await new_page.waitForXPath('// li //a[text() = "Tiếp"]');
//         await elNext?.click();
//         const elEnd = await new_page.waitForXPath('// li //a[text() = "Cuối"]');
//         await elEnd?.click();
//         let datatotalPage = await new_page.evaluate(() => document.body.outerHTML);
//         var $Page = cheerio.load(datatotalPage);
//       }else{

//         const elNext = await new_page.waitForXPath('// li //a[text() = "Tiếp"]');
//         await elNext?.click();
//         const elEnd = await new_page.waitForXPath(`// li //a[text() = "${page}"]`);
//         await elEnd?.click();
//         await new_page.waitForXPath(`// li[@class = "page-item active"] //a[text() = "${page}"]`)
//         await new_page.waitFor(1000);
//         let dataContent = await new_page.evaluate(() => document.body.outerHTML);
//         var $ = cheerio.load(dataContent);
//       }
//       await browser.close();
//   const CrawlerContent = $("div.list-news-cate > div.box-news-cate");
//   var page_Crawler = {}
//   let data = [];
//   for (let i = 0; i < CrawlerContent.length; i++) {
//     const data_avatar = $(`div.container > div > div > div.list-news-cate > div:nth-child(${i + 1}) > a > img`);
//     const title = $(`div.container > div > div > div.list-news-cate > div:nth-child(${i + 1}) > div > h3 > a`).text().trim();
//     const URL_title = $(`div.container > div > div > div.list-news-cate > div:nth-child(${i + 1}) > a`).attr('href');
//     const date = $(`div.container > div > div > div.list-news-cate > div:nth-child(${i + 1}) > div > span:nth-child(2)`).text().trim();
//     const des = $(`div.container > div > div > div.list-news-cate > div:nth-child(${i + 1}) > div > span:nth-child(3)`).text().replace(/\n|\r/g, "").trim();
//     const avatar = data_avatar.attr('src');
//     data.push({
//       avatar,
//       title,
//       URL_title,
//       date,
//       des,
//     });
//   }

//   if(page == '1'){
//     const valueMaxPage = Number($Page('div.pagination-primary > ul > li:nth-child(8) > a').text().trim());
//     page_Crawler = {
//       [page]: data,
//       totalPage : valueMaxPage}
//     }
//   else{
//     page_Crawler = {
//       [page]: data,}}

//   return await page_Crawler
// } catch (err) {
//   return(err);
// }
// }

const crawlDataLamDong = async (req, res) => {
  const { contentPage, page } = req?.body;
  if (!contentPage) {
    const bodData = await bodCrawler();
    const structureData = await structureCrawler();
    const workingScheduleData = await wsCrawler(1);
    const newsData = await CrawlerApi(newsCode, 1);
    const notificationData = await CrawlerApi(ntCode, 1);
    const information = {
      bod: bodData,
      structure: structureData,
      workingSchedule: workingScheduleData,
      news: newsData,
      notifications: notificationData,
    };
    res.send(information).status(200);
  } else {
    if (contentPage == "ws") {
      const workingScheduleData = await wsCrawler(page);
      const information = {
        workingSchedule: workingScheduleData,
      };
      res.send(information).status(200);
    }
    if (contentPage == "news") {
      const newsData = await CrawlerApi(newsCode, page);

      const information = {
        news: newsData,
      };
      res.send(information).status(200);
    }
    if (contentPage == "nt") {
      const notificationData = await CrawlerApi(ntCode, page);

      const information = {
        notifications: notificationData,
      };
      res.send(information).status(200);
    }
  }
};
module.exports = crawlDataLamDong;
