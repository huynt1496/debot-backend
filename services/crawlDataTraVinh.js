const rp = require("request-promise");
const cheerio = require("cheerio");

const bodURL = 'http://sgdtravinh.edu.vn/so-o-to-chuc';
const structureURL = 'http://sgdtravinh.edu.vn/so-o-to-chuc';
const wsURL = 'http://sgdtravinh.edu.vn//lich-lam-viec-so';
const ntURL = 'http://sgdtravinh.edu.vn/thong-bao';
const newsURL = 'http://sgdtravinh.edu.vn/tin-tuc-su-kien';


const options = (url, page) => {
  let newUrl = url;
  if (page) {
    newUrl = `${url}?page=${page}`;
  }
  return {
    uri: newUrl,
    transform: (body) => cheerio.load(body)
  }
};

const bodCrawler = async () => {
  try {
    var $ = await rp(options(bodURL));
  } catch (error) {
    return error;
  }
  const bodContent = $("#noidung ");

  const dataBodContent = {
    data: bodContent.html(),
    type: "html",
  };
  return dataBodContent;
};

const wsCrawler = async () => {
  try {
    var $ = await rp(options(wsURL));
  } catch (error) {
    return error;
  }

  const newsContent = $("div.journal-content-article");
  const dataNewContent = {
    data: newsContent.html(),
    type: "html",
  };

  return dataNewContent;
};

const structureCrawler = async () => {
  try {
    var $ = await rp(options(structureURL));
  } catch (error) {
    return error;
  }
  const digContent = $("#noidung ");

  const dataDigContent = {
    data: digContent.html(),
    type: "html",
  };
  return dataDigContent;
};

const notificationCrawler = async () => {
  try {
    var $ = await rp(options(ntURL));
  } catch (error) {
    return error;
  }
  const notificationContent = $("div.UI_Danhsachtin_Phantrang > div");
  var page_notification = {}
  let data = [];
  for (let i = 0; i < notificationContent.length; i++) {
    const data_page = $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > div > div > a > img`);
    if (data_page.length >= 1) {
      const URL = "http://sgdtravinh.edu.vn";
      const data_avatar = $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > div > div > a > img`);
      const title = $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > span > a`).text().trim();
      const URL_title = URL + $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > span > a`).attr('href');
      const date = $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > span > span`).text().trim();
      const des = $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > p`).text().trim();
      const avatar = data_avatar.attr('src');

      data.push({
        avatar,
        title,
        URL_title,
        date,
        des,
      });
    }
    else {
      const URL = "http://sgdtravinh.edu.vn";
      const title = $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > div > a`).text().trim();
      const URL_title = URL + $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > div > a`).attr('href');
      const date = $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > div > span`).text().trim();
      const des = $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > div > div`).text().trim();

      data.push({
        title,
        URL_title,
        date,
        des,
      });
    }

  }
  data.push({
    title: 'Xem thêm',
    URL_title: ntURL,
  });

  page_notification = {
    1: data,
  }
  return page_notification;
};

const newsCrawler = async () => {
  try {
    var $ = await rp(options(newsURL));
  } catch (error) {
    return error;
  }

  const newsContent = $("div.UI_Danhsachtin_Phantrang > div");
  var page_news = {}
  let data = [];
  for (let i = 0; i < newsContent.length; i++) {
    const data_page = $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > div > div > a > img`);
    if (data_page.length >= 1) {
      const URL = "http://sgdtravinh.edu.vn";
      const data_avatar = $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > div > div > a > img`);
      const title = $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > span > a`).text().trim();
      const URL_title = URL + $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > span > a`).attr('href');
      const date = $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > span > span`).text().trim();
      const des = $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > p`).text().trim();
      const avatar = data_avatar.attr('src');

      data.push({
        avatar,
        title,
        URL_title,
        date,
        des,
      });
    }
    else {
      const URL = "http://sgdtravinh.edu.vn";
      const title = $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > div > a`).text().trim();
      const URL_title = URL + $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > div > a`).attr('href');
      const date = $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > div > span`).text().trim();
      const des = $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > div > div`).text().trim();

      data.push({
        title,
        URL_title,
        date,
        des,
      });
    }

  }

  data.push({
    title: 'Xem thêm',
    URL_title: newsURL,
  });

  page_news = {
    1: data,
  }
  return page_news;
};

const crawlDataTraVinh = async (req, res) => {
  const { contentPage, page } = req?.body;
  if (!contentPage) {
    const results = await Promise.all([bodCrawler(), structureCrawler(), wsCrawler(), newsCrawler(), notificationCrawler()]);

    const information = {
      bod: results[0],
      structure: results[1],
      workingSchedule: results[2],
      news: results[3],
      notifications: results[4]
    };
    res.send(information).status(200);
  } else {
    res.send([]).status(200);
  }
}

module.exports = crawlDataTraVinh;