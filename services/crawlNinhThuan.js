const rp = require("request-promise");
const cheerio = require("cheerio");
const puppeteer = require('puppeteer');

const bodURL = 'http://ninhthuan.edu.vn/NINHTHUANEDU/4/467/2252/Co-cau-to-chuc';
const structureURL = 'http://ninhthuan.edu.vn/NINHTHUANEDU/4/467/2252/Co-cau-to-chuc';
const wsURL = 'http://ninhthuan.edu.vn/NINHTHUANEDU/4/467/2258/Lich-cong-tac';
const ntURL = 'http://ninhthuan.edu.vn/NINHTHUANEDU/4/467/38160/Thong-bao';
const newsURL = 'http://ninhthuan.edu.vn/NINHTHUANEDU/4/467/2229/Tin-tuc---Su-kien';

const options = async (url) => {
  const browser = await puppeteer.launch();
  const newPage = await browser.newPage();

  await newPage.goto(url, { waitUntil: 'networkidle0' });


  const datatotalPage = await newPage.evaluate(() => document.body.outerHTML);
  var $ = cheerio.load(datatotalPage);

  await browser.close();

  return $;
};

const bodCrawler = async () => {
  try {
    var $ = await options(bodURL);
  } catch (error) {
    return error;
  }
  const bodContent = $("div.ArticleInMenu");

  const dataBodContent = {
    data: bodContent.html(),
    type: "html",
  };
  return dataBodContent;
};

const structureCrawler = async () => {
  try {
    var $ = await options(structureURL);
  } catch (error) {
    return error;
  }
  const digContent = $("div.ArticleInMenu");
  const dataDigContent = {
    data: digContent.html(),
    type: "html",
  };
  return dataDigContent;
};

const newsCrawler = async () => {
  try {
    var $ = await options(newsURL);
  } catch (error) {
    return error;
  }
  const content = $("div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li");
  var page_news = {};
  let data = [];
  for (let i = 0; i < content.length; i++) {
    const data_avatar = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > a > img`);
    if (data_avatar.length >= 1) {
      const title = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > h2 > a`).text().trim();
      let URL_title = "http://ninhthuan.edu.vn/" + $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > h2 > a`).attr('href');
      const date = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td div.ArticleInMenu > ul > li:nth-child(${i + 1}) > div.Ngaydang`).text() || "[...]";
      const des = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > p`).text().trim();
      const avatar = "http://ninhthuan.edu.vn/" + data_avatar.attr('src');

      data.push({
        avatar,
        title,
        URL_title,
        date,
        des
      });
    } else {
      const title = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > h2 > a`).text().trim();
      let URL_title = "http://ninhthuan.edu.vn/" + $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > h2 > a`).attr('href');
      const date = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td div.ArticleInMenu > ul > li:nth-child(${i + 1}) > div.Ngaydang`).text() || "[...]";
      const des = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > p`).text().trim();

      data.push({
        title,
        URL_title,
        date,
        des
      });
    }
  }

  data.push({
    title: 'Xem thêm',
    URL_title: newsURL,
  });

  page_news = {
    1: data,
  }

  return page_news;
};
const notificationCrawler = async () => {
  try {
    var $ = await options(ntURL);
  } catch (error) {
    return error;
  }
  const content = $("div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li");
  var page_news = {};
  let data = [];
  for (let i = 0; i < content.length; i++) {
    const data_avatar = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > a > img`);
    if (data_avatar.length >= 1) {
      const title = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > h2 > a`).text().trim();
      let URL_title = "http://ninhthuan.edu.vn/" + $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > h2 > a`).attr('href');
      const date = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td div.ArticleInMenu > ul > li:nth-child(${i + 1}) > div.Ngaydang`).text() || "[...]";
      const des = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > p`).text().trim();
      const avatar = "http://ninhthuan.edu.vn/" + data_avatar.attr('src');

      data.push({
        avatar,
        title,
        URL_title,
        date,
        des
      });
    } else {
      const title = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > h2 > a`).text().trim();
      let URL_title = "http://ninhthuan.edu.vn/" + $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > h2 > a`).attr('href');
      const date = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td div.ArticleInMenu > ul > li:nth-child(${i + 1}) > div.Ngaydang`).text() || "[...]";
      const des = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > p`).text().trim();

      data.push({
        title,
        URL_title,
        date,
        des
      });
    }
  }

  data.push({
    title: 'Xem thêm',
    URL_title: ntURL,
  });

  page_news = {
    1: data,
  }

  return page_news;
};
const wsCrawler = async () => {
  try {
    var $ = await options(wsURL);
  } catch (error) {
    return error;
  }
  const content = $("div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li");
  var page_news = {};
  let data = [];
  for (let i = 0; i < content.length; i++) {
    const data_avatar = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > a > img`);
    if (data_avatar.length >= 1) {
      const title = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > h2 > a`).text().trim();
      let URL_title = "http://ninhthuan.edu.vn/" + $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > h2 > a`).attr('href');
      const date = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td div.ArticleInMenu > ul > li:nth-child(${i + 1}) > div.Ngaydang`).text() || "[...]";
      const des = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > p`).text().trim();
      const avatar = "http://ninhthuan.edu.vn/" + data_avatar.attr('src');

      data.push({
        avatar,
        title,
        URL_title,
        date,
        des
      });
    } else {
      const title = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > h2 > a`).text().trim();
      let URL_title = "http://ninhthuan.edu.vn/" + $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > h2 > a`).attr('href');
      const date = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td div.ArticleInMenu > ul > li:nth-child(${i + 1}) > div.Ngaydang`).text() || "[...]";
      const des = $(`div.ContentBanner > table > tbody > tr > td.LeftBar > table > tbody > tr > td > div.ArticleInMenu > ul > li:nth-child(${i + 1}) > p`).text().trim();

      data.push({
        title,
        URL_title,
        date,
        des
      });
    }
  }

  data.push({
    title: 'Xem thêm',
    URL_title: wsURL,
  });

  page_news = {
    1: data,
  }

  return page_news;
};

const crawlNinhThuan = async (req, res) => {
  const { contentPage, page } = req?.body;
  if (!contentPage) {
    const results = await Promise.all([bodCrawler(), structureCrawler(), wsCrawler(wsURL, 1), newsCrawler(newsURL, 1), notificationCrawler(ntURL, 1)]);

    const information = {
      bod: results[0],
      structure: results[1],
      workingSchedule: results[2],
      news: results[3],
      notifications: results[4]
    };
    res.send(information).status(200);
  }
  else {
    res.send(information).status(200);
  }
}
module.exports = crawlNinhThuan;