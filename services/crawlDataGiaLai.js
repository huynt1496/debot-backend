const rp = require("request-promise");
const puppeteer = require('puppeteer');
const cheerio = require("cheerio");
const { type } = require("express/lib/response");

const URL = "http://gialai.edu.vn/";
const bodURL = 'http://gialai.edu.vn/ban-giam-doc';
const structureURL = 'http://gialai.edu.vn/co-cau-to-chuc';
const wsURL = 'http://gialai.edu.vn/tiep-cong-dan/lich-lam-viec-lanh-dao';
const ntURL = 'http://gialai.edu.vn/van-ban-cong-van/thong-bao-gioi-thieu';
const newsURL = 'http://gialai.edu.vn/tin-tuc-su-kien';

const options = async (url) => {
  const browser = await puppeteer.launch();
  const newPage = await browser.newPage();

  await newPage.goto(url);

  const datatotalPage = await newPage.evaluate(() => document.body.outerHTML);
  var $ = cheerio.load(datatotalPage);

  await browser.close();

  return $;
};

const bodCrawler = async () => {
  try {
    var $ = await options(bodURL);
  } catch (error) {
    return error;
  }
  const tableContent = $("#article15>.post-content>table");
  let data = [];
  for (let i = 1; i < tableContent.length + 1; i++) {
    const col2Length = $(`#article15>.post-content>table:nth-child(${2 * i})>tbody>tr>td:last-child>img`);
    if (col2Length.length > 0) {

      const info = $(`#article15>.post-content>table:nth-child(${2 * i})>tbody>tr:nth-child(1)`).text().trim() + "\n" + "\n" + $(`#article15>.post-content>table:nth-child(${2 * i})>tbody>tr:nth-child(2)`).text().trim() + "\n" + "\n" + $(`#article15>.post-content>table:nth-child(${2 * i})>tbody>tr:nth-child(3)`).text().trim() + "\n" + "\n" + $(`#article15>.post-content>table:nth-child(${2 * i})>tbody>tr:nth-child(4)`).text().trim() + "\n" + "\n" + $(`#article15>.post-content>table:nth-child(${2 * i})>tbody>tr:nth-child(5)`).text().trim();

      const col2 = $(`#article15>.post-content>table:nth-child(${2 * i})>tbody>tr>td:last-child>img`);
      const avatar = `${URL}${col2.attr('src')}`;

      data.push({
        avatar,
        info,
      });
    }
    else {

      const info = $(`#article15>.post-content>table:nth-child(${2 * i})>tbody>tr:nth-child(1)`).text().trim() + "\n" + "\n" + $(`#article15>.post-content>table:nth-child(${2 * i})>tbody>tr:nth-child(2)`).text().trim() + "\n" + "\n" + $(`#article15>.post-content>table:nth-child(${2 * i})>tbody>tr:nth-child(3)`).text().trim() + "\n" + "\n" + $(`#article15>.post-content>table:nth-child(${2 * i})>tbody>tr:nth-child(4)`).text().trim() + "\n" + "\n" + $(`#article15>.post-content>table:nth-child(${2 * i})>tbody>tr:nth-child(5)`).text().trim();
      const col2 = $(`#article15>.post-content>table:nth-child(${2 * i})>tbody>tr>td:last-child>span>span>img`);
      const avatar = `${URL}${col2.attr('src')}`;

      data.push({
        avatar,
        info,

      });
    }

  }

  return data;
};

const structureCrawler = async () => {
  try {
    var $ = await options(structureURL);
  } catch (error) {
    return error;
  }
  const digContent = $("#section15> #article15 >div>p>img");

  const dataDigContent = {
    data: URL + digContent?.attr('src'),
    type: "img",
  };
  return dataDigContent;
};
;

const wsCrawler = async () => {
  try {
    var $ = await options(wsURL);
  } catch (error) {
    return error;
  }

  const newsContent = $("#module16");
  const rep = `<h2>&lt;&lt; Tuần trước</h2>`



  const rep2 = `<h2> <a rel="noreferrer" target="_blank" \ href="http://gialai.edu.vn/tiep-cong-dan/lich-lam-viec-lanh-dao" \ style="color:black; margin:40%; font-size: 20px;     border: 2px solid;
 text-decoration: none;
 box-shadow: 0 3px tomato;
 padding: 4px;
 color: black;">\
 Xem thêm các tuần khác</a></h2>`
  const dataNewContent = {
    data: newsContent.html().replace(rep, rep2),
    type: "html"
  };

  return dataNewContent;
};
const notificationCrawler = async () => {
  try {
    var $ = await options(ntURL);
  } catch (error) {
    return error;
  }

  const ContentNT = $("#module15 >div:nth-child(2)>table>tbody>tr");
  var page_news = {};
  let data = [];
  for (let i = 0; i < ContentNT.length; i++) {
    const title = $(`#module15 >div:nth-child(2)>table>tbody>tr:nth-child(${i + 1})>td:nth-child(2)>a`).text().trim();
    const URL_title = URL + $(`#module15 >div:nth-child(2)>table>tbody>tr:nth-child(${i + 1})>td:nth-child(2)>a`).attr('href');
    const date = $(`#module15 >div:nth-child(2)>table>tbody>tr:nth-child(${i + 1})>td:nth-child(5)`).text().trim();
    data.push({
      title,
      URL_title,
      date
    });
  }
  data.push({
    title: 'Xem thêm',
    URL_title: ntURL,
  });

  page_news = {
    1: data,
  }


  return page_news;
};

const newsCrawler = async () => {
  try {
    var $ = await options(newsURL);
  } catch (error) {
    return error;
  }

  const Content = $("#section15 > article");
  var page_news = {};
  let data = [];
  for (let i = 0; i < Content.length; i++) {
    const data_avatar = $(`#section15 > article:nth-child(${i + 1}) > div > div > figure > a > img`).attr('src').replaceAll("/publish/thumbnail/", "http://gialai.edu.vn//publish/thumbnail/");
    const title = $(`#section15 > article:nth-child(${i + 1}) > div > div > div > h4 > a`).text().trim();
    let URL_title = URL + $(`#section15 > article:nth-child(${i + 1}) > div > div > div > h4 > a`).attr('href');
    const date = $(`#section15 > article:nth-child(${i + 1}) > div > div > div > span.time-v`).text().trim();
    const seen = $(`#section15 > article:nth-child(${i + 1}) > div > div > div > span.luotxem`).text().trim();
    const des = $(`#section15 > article:nth-child(${i + 1}) > div > div > div:nth-child(3)`).text().trim();

    const avatar = data_avatar;

    data.push({
      avatar,
      title,
      URL_title,
      date,
      seen,
      des,
    });
  }

  data.push({
    title: 'Xem thêm',
    URL_title: ntURL,
  });

  page_news = {
    1: data,
  }

  return page_news;
};

const crawlDataGiaLai = async (req, res) => {
  const { contentPage } = req?.body;
  if (!contentPage) {
    const results = await Promise.all([bodCrawler(), structureCrawler(), wsCrawler(), newsCrawler(), notificationCrawler()]);
    const information = {
      bod: results[0],
      structure: results[1],
      workingSchedule: results[2],
      news: results[3],
      notifications: results[4]
    };
    res.send(information).status(200);
  } else {
    res.send([]).status(200);
  }
}

module.exports = crawlDataGiaLai;