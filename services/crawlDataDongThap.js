const rp = require("request-promise");
const puppeteer = require('puppeteer');
const cheerio = require("cheerio");

const URL = "http://dongthap.edu.vn/";
const bodURL = 'http://dongthap.edu.vn/to-chuc/ban-giam-doc';
const structureURL = 'http://dongthap.edu.vn/so-do-to-chuc';
const wsURL = 'http://lichhop.dongthap.gov.vn/sgd/';
const ntURL = 'http://dongthap.edu.vn/van-ban';
const newsURL = 'http://dongthap.edu.vn/tin-tuc2';

const options = async (url) => {
  const browser = await puppeteer.launch();
  const newPage = await browser.newPage();

  await newPage.goto(url, { waitUntil: 'networkidle0' });

  const datatotalPage = await newPage.evaluate(() => document.body.outerHTML);
  var $ = cheerio.load(datatotalPage);

  await browser.close();

  return $;
};

const bodCrawler = async () => {
  try {
    var $ = await options(bodURL);
  } catch (error) {
    return error;
  }
  const tableContent = $("#section-person11 > div");
  let data = [];
  for (let i = 0; i < tableContent.length - 2; i++) {
    const data_avatar = $(`#section-person11 > div:nth-child(${2 * i + 1}) > ul > li > div > div > figure > img`);
    const info = $(`#section-person11 > div:nth-child(${2 * i + 1}) > ul > li > div > div > ul`).text().trim().replace("Giới thiệu sơ bộ:", "");
    const title = $(`#section-person11 > div:nth-child(${2 * i + 1}) > ul > li > div > div > ul > li > div > h2 > a`).text().trim();
    const URL_Info = URL + $(`#section-person11 > div:nth-child(${2 * i + 1}) > ul > li > div > div > ul > li > div > h2 > a`).attr('href');
    const avatar = URL + data_avatar.attr('src');

    data.push({
      avatar,
      info,
      title,
      URL_Info,
    });
  }
  return data;
};

const structureCrawler = async () => {
  try {
    var $ = await options(structureURL);
  } catch (error) {
    return error;
  }
  const digContent = $("#section8 > div > div > p > img");

  const dataDigContent = {
    data: URL + digContent?.attr('src'),
    type: "img",
  };
  return dataDigContent;
};

const wsCrawler = async () => {
  try {
    var $ = await options(wsURL);
  } catch (error) {
    return error;
  }

  const newsContent = $("div.panel.panel-primary");
  const dataNewContent = {
    data: newsContent.html() + '<br/><a rel="noreferrer" target="_blank" \
                                href="http://lichhop.dongthap.gov.vn/sgd/" \
                                style=" margin:40%; font-size: 20px;">\
                                Xem thêm các tuần khác</a>',
    type: "html",
  };

  return dataNewContent;
};

const notificationCrawler = async () => {
  try {
    var $ = await options(ntURL);
  } catch (error) {
    return error;
  }

  const ContentNT = $("table > tbody > tr");
  var page_notification = {};
  let data = [];
  for (let i = 0; i < ContentNT.length; i++) {
    const title = $(`table > tbody > tr:nth-child(${i + 1}) > td:nth-child(4) > a > p`).text().trim();
    let URL_title = URL + $(`table > tbody > tr:nth-child(${i + 1}) > td:nth-child(4) > a`).attr('href');
    let date = $(`table > tbody > tr:nth-child(${i + 1}) > td:nth-child(3)`).text().trim();

    data.push({
      title,
      URL_title,
      date,
    });
  }

  data.push({
    title: 'Xem thêm',
    URL_title: ntURL,
  });

  page_notification = {
    1: data,
  }

  return page_notification;
};

const newsCrawler = async () => {
  try {
    var $ = await options(newsURL);
  } catch (error) {
    return error;
  }

  const Content = $("#section11 > article ");
  var page_news = {};
  let data = [];
  for (let i = 0; i < Content.length; i++) {
    const data_avatar = $(`#section11 > article:nth-child(${i + 1}) > div > div > figure > a > img `);
    const title = $(`#section11 > article:nth-child(${i + 1}) > div > div  > div > h4 > a`).text().trim();
    let URL_title = URL + $(`#section11 > article:nth-child(${i + 1}) > div > div  > div > h4 > a`).attr('href');
    const date = $(`#section11 > article:nth-child(${i + 1}) > div > div  > div > span > time`).text().trim();
    const user = $(`#section11 > article:nth-child(${i + 1}) > div > div  > div > span > span`).text().trim();
    const des = $(`#section11 > article:nth-child(${i + 1}) > div > div  > div > p`).text().trim();
    const avatar = URL + data_avatar.attr('src');

    data.push({
      avatar,
      title,
      URL_title,
      date,
      user,
      des,
    });
  }

  data.push({
    title: 'Xem thêm',
    URL_title: newsURL,
  });

  page_news = {
    1: data,
  }

  return page_news;
};

const crawlDataDongThap = async (req, res) => {
  const { contentPage, page } = req?.body;
  if (!contentPage) {
    const results = await Promise.all([bodCrawler(), structureCrawler(), wsCrawler(), newsCrawler(), notificationCrawler()]);

    const information = {
      bod: results[0],
      structure: results[1],
      workingSchedule: results[2],
      news: results[3],
      notifications: results[4]
    };

    res.send(information).status(200);
  }
  else {
    res.send(information).status(200);
  }
}

module.exports = crawlDataDongThap;