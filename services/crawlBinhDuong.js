const rp = require("request-promise");
const cheerio = require("cheerio");
const puppeteer = require('puppeteer');

const bodURL = 'https://binhduong.edu.vn/lanh-dao-so-gddt-binh-duong.html';
const wsURL = 'https://binhduong.edu.vn/lich-lam-viec';
const ntURL = 'https://binhduong.edu.vn/thong-bao-van-phong';
const newsURL = 'https://binhduong.edu.vn/so-giao-duc-va-dao-tao';

const options = async (url) => {
  const browser = await puppeteer.launch();
  const newPage = await browser.newPage();

  await newPage.goto(url);

  const datatotalPage = await newPage.evaluate(() => document.body.outerHTML);
  var $ = cheerio.load(datatotalPage);

  await browser.close();

  return $;
};

const bodCrawler = async () => {
  try {
    var $ = await options(bodURL);
  } catch (error) {
    return error;
  }
  const bodContent = $("div.article-content");

  const dataBodContent = {
    data: bodContent.html(),
    type: "html",
  };
  return dataBodContent;
};

const wsCrawler = async () => {
  try {
    var $ = await options(wsURL);
  } catch (error) {
    return error;
  }

  const wsContent = $("div.bg-light > div > div > div > div.d-flex");
  var page_ws = {};
  let data = [];
  for (let i = 0; i < wsContent.length; i++) {
    const title = $(`div.bg-light > div > div > div >div.d-flex:nth-child(${i + 1}) > a`).text().trim();
    const URL_title = "https://binhduong.edu.vn/" + $(`div.bg-light > div > div > div >div.d-flex:nth-child(${i + 1}) > a`).attr('href');
    data.push({
      title,
      URL_title,
    });
  }

  data.push({
    title: 'Xem thêm',
    URL_title: wsURL,
  });

  page_ws = {
    1: data,
  }

  return page_ws;
};

const structureCrawler = async () => {

  const dataDigContent = {
    data: `<h3>Không có dữ liệu</h3>`,
    type: "html",
  };
  return dataDigContent;
};

const notificationCrawler = async () => {
  try {
    var $ = await options(ntURL);
  } catch (error) {
    return error;
  }
  const notificationContent = $("div.col-md-8 > div > table > tbody >tr");
  var page_notification = {}
  let data = [];
  for (let i = 0; i < notificationContent.length; i++) {
    const URL = "https://binhduong.edu.vn";
    const title = $(`div.col-md-8 > div > table > tbody >tr:nth-child(${i + 1}) > td:nth-child(2) > a > span`).text().trim();
    let URL_title = URL + $(`div.col-md-8 > div > table > tbody >tr:nth-child(${i + 1}) > td:nth-child(2) > a`).attr('href');
    const date = $(`div.col-md-8 > div > table > tbody >tr:nth-child(${i + 1}) > td:nth-child(1)`).text().trim();
    data.push({
      title,
      date,
      URL_title
    });
  }

  data.push({
    title: 'Xem thêm',
    URL_title: 'https://binhduong.edu.vn/thong-bao',
  });

  page_notification = {
    1: data,
  }

  return page_notification;
};

const newsCrawler = async () => {
  try {
    var $ = await options(newsURL);
  } catch (error) {
    return error;
  }

  const newsContent = $("div.col-md-8 > div.old-articles > div");
  var page_news = {}
  let data = [];
  for (let i = 0; i < newsContent.length; i++) {
    const data_page = $(`div.UI_Danhsachtin_Phantrang > div:nth-child(${i + 1}) > div > div > a > img`);
    const URL = "https://binhduong.edu.vn";
    const data_avatar = $(`div.col-md-8 > div.old-articles > div:nth-child(${i + 1}) > div > div.col-lg-4 > div > a > img`);
    const title = $(`div.col-md-8 > div.old-articles > div:nth-child(${i + 1}) > div > div.col-lg-8 > div > a`).text().trim();
    const URL_title = URL + $(`div.col-md-8 > div.old-articles > div:nth-child(${i + 1}) > div > div.col-lg-8 > div > a`).attr('href');
    const date = $(`div.col-md-8 > div.old-articles > div:nth-child(${i + 1}) > div > div.col-lg-8 > div > div > span:first-child`).text().trim();
    const des = $(`div.col-md-8 > div.old-articles > div:nth-child(${i + 1}) > div > div.col-lg-8 > div > p`).text().trim();
    const seen = $(`div.col-md-8 > div.old-articles > div:nth-child(${i + 1}) > div > div.col-lg-8 > div > div > span > span`).text().trim();
    const avatar = data_avatar.attr('src');

    data.push({
      avatar,
      title,
      URL_title,
      date,
      des,
      seen
    });
  }

  data.push({
    title: 'Xem thêm',
    URL_title: 'https://binhduong.edu.vn/tong-hop',
  });

  page_news = {
    1: data,
  }

  return page_news;
};

const crawlBinhDuong = async (req, res) => {
  const { contentPage, page } = req?.body;
  if (!contentPage) {
    const results = await Promise.all([bodCrawler(), structureCrawler(), wsCrawler(), newsCrawler(), notificationCrawler()]);

    const information = {
      bod: results[0],
      structure: results[1],
      workingSchedule: results[2],
      news: results[3],
      notifications: results[4]
    };
    res.send(information).status(200);
  } else {
    res.send([]).status(200);
  }
}
module.exports = crawlBinhDuong;