const rp = require("request-promise");
const puppeteer = require('puppeteer');
const cheerio = require("cheerio");
const { type } = require("express/lib/response");

const URL = "http://bariavungtau.edu.vn/";
const bodURL = 'http://bariavungtau.edu.vn/SoGDDT/4/1947/6488/Gioi-thieu/';
const structureURL = 'http://bariavungtau.edu.vn/SoGDDT/4/1947/7990/So-do-to-chuc/';
const wsURL = 'http://bariavungtau.edu.vn/SoGDDT/4/1947/6494/Lich-Cong-Tac-Tuan/';
const ntURL = 'http://bariavungtau.edu.vn/SoGDDT/4/1947/6486/VAN-PHONG-SO/';
const newsURL = 'http://bariavungtau.edu.vn/SoGDDT/4/1947/8050/Thong-tin-tuyen-truyen/';

const options = async (url) => {
  const browser = await puppeteer.launch();
  const newPage = await browser.newPage();

  await newPage.goto(url);

  const datatotalPage = await newPage.evaluate(() => document.body.outerHTML);
  var $ = cheerio.load(datatotalPage);

  await browser.close();

  return $;
};

const bodCrawler = async () => {

  try {
    var $ = await options(bodURL);
  } catch (error) {
    return error;
  }
  const rep2 = `<h2> <a rel="noreferrer" target="_blank" \ href="http://bariavungtau.edu.vn/SoGDDT/4/1947/6488/Gioi-thieu/" \ style="color:black;  font-size: 18px;     border: 2px solid;
   text-decoration: none;
   box-shadow: 0 3px tomato;
   padding: 4px;
   color: black;">\
   Xem Thông Tin Chi Tiết Các Cấp Lãnh Đạo</a></h2>`
  const dataNewContent = {
    data: rep2,
    type: "html"
  };
  return dataNewContent;
};

const structureCrawler = async () => {
  try {
    var $ = await options(structureURL);
  } catch (error) {
    return error;
  }
  const digContent = $(".div-map> .organizational-map>li");
  const dataDigContent = {
    data: digContent.html(),
    type: "html",
  };
  return dataDigContent;
};



const wsCrawler = async () => {
  try {
    var $ = await options(wsURL);
  } catch (error) {
    return error;
  }

  const newsContent = $(".SimpleCalendar> .container ");
  const rep = $(".SimpleCalendar >.zonetitle");
  const rep1 = $(".SimpleCalendar >.DateShow");
  const rep2 = `<h2> <a rel="noreferrer" target="_blank" \ href="http://bariavungtau.edu.vn/SoGDDT/4/1947/6488/Gioi-thieu/" \ style="color:black;  font-size: 14px;   
   text-decoration: none;
   box-shadow: 0 2px tomato;
   padding: 4px;
   color: blue;">\
   Xem Thêm Thông Tin </a></h2>`

  const dataNewContent = {
    data: rep.text().trim() + "<br/>" + rep1.text().trim() + rep2 + newsContent.html(),
    type: "html"
  };

  return dataNewContent;
};
const notificationCrawler = async () => {
  try {
    var $ = await options(ntURL);
  } catch (error) {
    return error;
  }

  const ContentNT = $("#ctrl_35308_30_lvwArticleInMenu_itemPlaceholderContainer >li");
  var page_news = {};
  let data = [];
  for (let i = 0; i < ContentNT.length; i++) {
    const title = $(`#ctrl_35308_30_lvwArticleInMenu_itemPlaceholderContainer >li:nth-child(${i + 1})>h2`).text().trim();
    const URL_title = URL + $(`#ctrl_35308_30_lvwArticleInMenu_itemPlaceholderContainer >li:nth-child(${i + 1})>h2>a`).attr('href');
    const avatar = URL + $(`#ctrl_35308_30_lvwArticleInMenu_itemPlaceholderContainer >li:nth-child(${i + 1})>a >img`).attr('src');
    data.push({
      title,
      URL_title,
      avatar
    });
  }
  data.push({
    title: 'Xem thêm',
    URL_title: ntURL,
  });

  page_news = {
    1: data,
  }


  return page_news;
};

const newsCrawler = async () => {
  try {
    var $ = await options(newsURL);
  } catch (error) {
    return error;
  }

  const ContentNT = $("#ctrl_35308_30_lvwArticleInMenu_itemPlaceholderContainer >li");
  var page_news = {};
  let data = [];
  for (let i = 0; i < ContentNT.length; i++) {
    const title = $(`#ctrl_35308_30_lvwArticleInMenu_itemPlaceholderContainer >li:nth-child(${i + 1})>h2`).text().trim();
    const URL_title = URL + $(`#ctrl_35308_30_lvwArticleInMenu_itemPlaceholderContainer >li:nth-child(${i + 1})>h2>a`).attr('href');
    const avatar = URL + $(`#ctrl_35308_30_lvwArticleInMenu_itemPlaceholderContainer >li:nth-child(${i + 1})>a >img`).attr('src');
    data.push({
      title,
      URL_title,
      avatar
    });
  }

  data.push({
    title: 'Xem thêm',
    URL_title: ntURL,
  });

  page_news = {
    1: data,
  }

  return page_news;
};

const crawlDataBRVT = async (req, res) => {
  const { contentPage } = req?.body;
  if (!contentPage) {
    const results = await Promise.all([bodCrawler(), structureCrawler(), wsCrawler(), newsCrawler(), notificationCrawler()]);
    const information = {
      bod: results[0],
      structure: results[1],
      workingSchedule: results[2],
      news: results[3],
      notifications: results[4]
    };
    res.send(information).status(200);
  } else {
    res.send([]).status(200);
  }
}

module.exports = crawlDataBRVT;