const rp = require("request-promise");
const puppeteer = require('puppeteer');
const cheerio = require("cheerio");

const URL = "http://cantho.edu.vn";
const bodURL = 'http://cantho.edu.vn/gioi-thieu/co-cau-to-chuc/lanh-dao-so';
const wsURL = 'http://cantho.edu.vn/lich-tiep-cong-dan';
const ntURL = 'http://cantho.edu.vn/van-ban/giay-moi-thong-bao';
const newsURL = 'http://cantho.edu.vn/tin-tuc';

const options = async (url) => {
  const browser = await puppeteer.launch();
  const newPage = await browser.newPage();

  await newPage.goto(url, { waitUntil: 'networkidle0' });

  const datatotalPage = await newPage.evaluate(() => document.body.outerHTML);
  var $ = cheerio.load(datatotalPage);

  await browser.close();

  return $;
};

const bodCrawler = async () => {
  try {
    var $ = await options(bodURL);
  } catch (error) {
    return error;
  }
  const bodContent = $("#section-person12 > div");
  let data = [];
  for (let i = 0; i < bodContent.length - 8; i++) {
    const data_avatar = $(`#section-person12 > div:nth-child(${3 * i + 1}) > div > div:nth-child(2) > figure > img`);
    const info = $(`#section-person12 > div:nth-child(${3 * i + 1}) > div > div > div `).text().trim().replace("\n\n>> Tiểu sử tóm tắt\n>> Lĩnh vực công việc phụ trách", "");
    const avatar = URL + data_avatar.attr('src');
    const title = $(`#section-person12 > div:nth-child(${3 * i + 1}) > div > div > div > div > a > b`).text().trim();
    const URL_Info = URL + $(`#section-person12 > div:nth-child(${3 * i + 1}) > div > div > div > div > a`).attr('href');

    data.push({
      avatar,
      info,
      URL_Info,
      title,
    });
  }
  return data;
};

const structureCrawler = async () => {
  const dataDigContent = {
    data: `<h3>Không có dữ liệu</h3>`,
    type: "html",
  };
  return dataDigContent;
};

const notificationCrawler = async () => {
  try {
    var $ = await options(ntURL);
  } catch (error) {
    return error;
  }
  const Content = $("#module12 > div.table-responsive > table > tbody > tr");
  var page_notification = {};
  let data = [];
  for (let i = 0; i < Content.length; i++) {
    const title = $(`#module12 > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td.tg-yw4l > a`).text().trim();
    let URL_title = URL + $(`#module12 > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td.tg-yw4l > a`).attr('href');
    const date = "Ngày ban hành " + $(`#module12 > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(5)`).text().trim();
    data.push({
      title,
      URL_title,
      date,
    });
  }

  data.push({
    title: 'Xem thêm',
    URL_title: ntURL,
  });

  page_notification = {
    1: data,
  }

  return page_notification;
};

const newsCrawler = async () => {
  try {
    var $ = await options(newsURL);
  } catch (error) {
    return error;
  }

  const Content = $("#section12 > article");
  var page_news = {};
  let data = [];

  for (let i = 0; i < Content.length; i++) {
    const title = $(`#section12 > article:nth-child(${i + 1}) > h4 > a`).text().trim();
    let URL_title = URL + $(`#section12 > article:nth-child(${i + 1}) > h4 > a`).attr('href');
    const date = $(`#section12 > article:nth-child(${i + 1}) > div`).text().trim();
    data.push({
      title,
      URL_title,
      date,
    });
  }

  data.push({
    title: 'Xem thêm',
    URL_title: newsURL,
  });

  page_news = {
    1: data,
  }

  return page_news;
};

const wsCrawler = async () => {
  try {
    var $ = await options(wsURL);
  } catch (error) {
    return error;
  }

  const Content = $("#section12 > article");
  var page_ws = {};
  let data = [];

  for (let i = 0; i < Content.length; i++) {
    const title = $(`#section12 > article:nth-child(${i + 1}) > h4 > a`).text().trim();
    let URL_title = URL + $(`#section12 > article:nth-child(${i + 1}) > h4 > a`).attr('href');
    const date = $(`#section12 > article:nth-child(${i + 1}) > div`).text().trim();
    data.push({
      title,
      URL_title,
      date,
    });
  }

  data.push({
    title: 'Xem thêm',
    URL_title: wsURL,
  });

  page_ws = {
    1: data,
  }

  return page_ws;
};

const crawlDataCanTho = async (req, res) => {
  const { contentPage, page } = req?.body;
  if (!contentPage) {
    const results = await Promise.all([bodCrawler(), structureCrawler(), wsCrawler(), newsCrawler(), notificationCrawler()]);
    const information = {
      bod: results[0],
      structure: results[1],
      workingSchedule: results[2],
      news: results[3],
      notifications: results[4]
    };

    res.send(information).status(200);
  }
  else {
    res.send(information).status(200);
  }
}

module.exports = crawlDataCanTho;