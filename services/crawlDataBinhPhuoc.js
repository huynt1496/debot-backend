const rp = require("request-promise");
const cheerio = require("cheerio");
const puppeteer = require('puppeteer');

const URL = 'http://binhphuoc.edu.vn/';
const wsURL = 'http://binhphuoc.edu.vn/SoGDDTBINHPHUOC/97/1505/3291/Lich-lam-viec/  ';
const ntURL = 'http://binhphuoc.edu.vn/SoGDDTBINHPHUOC/97/1507/3306/116032/Trao-doi/6-hanh-vi-giao-vien-khong-duoc-lam.html';
const newsURL = 'http://binhphuoc.edu.vn/SoGDDTBINHPHUOC/97/1507/3303/156106/Tin-tuc---Su-kien/Cong-bo-bao-cao-TEMIS-nam-2021.html';

const options = async (url) => {
  const browser = await puppeteer.launch();
  const newPage = await browser.newPage();

  await newPage.goto(url);

  const datatotalPage = await newPage.evaluate(() => document.body.outerHTML);
  var $ = cheerio.load(datatotalPage);

  await browser.close();

  return $;
};

const bodCrawler = async () => {
  const dataDigContent = {
    data: `<h3>Không có dữ liệu</h3>`,
    type: "html",
  };
  return dataDigContent;
};

const structureCrawler = async () => {
  const dataDigContent = {
    data: `<h3>Không có dữ liệu</h3>`,
    type: "html",
  };
  return dataDigContent;
};

const wsCrawler = async () => {
  try {
    var $ = await options(wsURL);
  } catch (error) {
    return error;
  }

  var page_ws = {};
  let data = [];

  data.push({
    title: 'Xem thêm',
    URL_title: wsURL,
  });

  page_ws = {
    1: data,
  }
  return page_ws;
};

const notificationCrawler = async () => {
  try {
    var $ = await options(ntURL);
  } catch (error) {
    return error;
  }

  const notificationContent = $("ul.ArticleList > li");
  var page_notification = {};
  let data = [];
  for (let i = 0; i < notificationContent.length - 10; i++) {
    const title = $(`ul.ArticleList > li:nth-child(${2 * i + 1}) > a > div:nth-child(1)`).text().trim();
    let URL_title = URL + $(`ul.ArticleList > li:nth-child(${2 * i + 1}) > a`).attr('href');
    const date = $(`ul.ArticleList > li:nth-child(${2 * i + 1}) > a > div:nth-child(2)`).text().trim();
    data.push({
      title,
      URL_title,
      date,
    });
  }
  data.push({
    title: 'Xem thêm',
    URL_title: ntURL,
  });

  page_notification = {
    1: data,
  }
  return page_notification;
};

const newsCrawler = async () => {
  try {
    var $ = await options(newsURL);
  } catch (error) {
    return error;
  }

  const newsContent = $("ul.ArticleList > li");
  var page_news = {};
  let data = [];
  for (let i = 0; i < newsContent.length - 10; i++) {
    const title = $(`ul.ArticleList > li:nth-child(${2 * i + 1}) > a > div:nth-child(1)`).text().trim();
    let URL_title = URL + $(`ul.ArticleList > li:nth-child(${2 * i + 1}) > a`).attr('href');
    const date = $(`ul.ArticleList > li:nth-child(${2 * i + 1}) > a > div:nth-child(2)`).text().trim();
    data.push({
      title,
      URL_title,
      date,
    });
  }
  data.push({
    title: 'Xem thêm',
    URL_title: newsURL,
  });
  page_news = {
    1: data,
  }

  return page_news;
};

const crawlDataBinhPhuoc = async (req, res) => {
  const { contentPage, page } = req?.body;
  if (!contentPage) {
    const results = await Promise.all([bodCrawler(), structureCrawler(), wsCrawler(), newsCrawler(), notificationCrawler()]);

    const information = {
      bod: results[0],
      structure: results[1],
      workingSchedule: results[2],
      news: results[3],
      notifications: results[4]
    };
    res.send(information).status(200);
  } else {
    res.send([]).status(200);
  }
}

module.exports = crawlDataBinhPhuoc;