const rp = require("request-promise");
const puppeteer = require('puppeteer');
const cheerio = require("cheerio");

const URL = "https://vinhlong.edu.vn";
const bodURL = 'https://vinhlong.edu.vn/co-cau-to-chuc/van-phong';
const structureURL = 'https://vinhlong.edu.vn/gioi-thieu/chuc-nang-nhiem-vu';
const wsURL = 'https://vinhlong.edu.vn';
const ntURL = 'https://vinhlong.edu.vn/van-ban-chi-dao-dieu-hanh';
const newsURL = 'https://vinhlong.edu.vn/tin-tuc';

const options = async (url) => {
  const browser = await puppeteer.launch();
  const newPage = await browser.newPage();

  await newPage.goto(url, { waitUntil: 'networkidle0' });

  const datatotalPage = await newPage.evaluate(() => document.body.outerHTML);
  var $ = cheerio.load(datatotalPage);

  await browser.close();

  return $;
};

const bodCrawler = async () => {
  try {
    var $ = await options(bodURL);
  } catch (error) {
    return error;
  }
  const digContent = $("#article8 >.post-content ");

  const dataDigContent = {
    data: digContent.html(),
    type: "html",
  };
  return dataDigContent;
};

const structureCrawler = async () => {
  try {
    var $ = await options(structureURL);
  } catch (error) {
    return error;
  }
  const digContent = $("#article8");

  const dataDigContent = {
    data: digContent.html(),
    type: "html",
  };
  return dataDigContent;
};

const wsCrawler = async () => {
  try {
    var $ = await options(wsURL);
  } catch (error) {
    return error;
  }
  var page_ws = {};
  let data = [];
  const URL_title = URL + $(`#categoryType262.simple-menu > li:nth-child(11) > a `).attr('href');
  data.push({
    title: 'Xem Lịch Công Tác',
    URL_title,
  });

  page_ws = {
    1: data,
  }

  return page_ws;
};

const notificationCrawler = async () => {
  try {
    var $ = await options(ntURL);
  } catch (error) {
    return error;
  }

  const ContentNT = $("#section6 > div.legal-document-listType1");
  var page_notification = {};
  let data = [];
  for (let i = 0; i < ContentNT.length; i++) {
    const title = $(`#section6 > div.legal-document-listType1:nth-child(${2 * i + 2}) > div > a`).text().trim();
    let URL_title = URL + $(`#section6 > div.legal-document-listType1:nth-child(${2 * i + 2}) > div > a`).attr('href');

    data.push({
      title,
      URL_title,
    });
  }
  data.push({
    title: 'Xem thêm',
    URL_title: ntURL,
  });

  page_notification = {
    1: data,
  }

  return page_notification;
};

const newsCrawler = async () => {
  try {
    var $ = await options(newsURL);
  } catch (error) {
    return error;
  }

  const Content = $("#section6 > article");
  var page_news = {};
  let data = [];
  for (let i = 0; i < Content.length; i++) {
    const data_avatar = $(`#section6 > article:nth-child(${i + 1}) > div > div > figure > a > img`);
    const title = $(`#section6 > article:nth-child(${i + 1}) > div > div > div > h4 > a`).text().trim();
    let URL_title = URL + $(`#section6 > article:nth-child(${i + 1}) > div > div > div > h4 > a`).attr('href');
    const date = $(`#section6 > article:nth-child(${i + 1}) > div > div > div > span.time-v`).text().trim();
    const seen = $(`#section6 > article:nth-child(${i + 1}) > div > div > div > span.luotxem`).text().trim();
    const des = $(`#section6 > article:nth-child(${i + 1}) > div > div > div:nth-child(3)`).text().trim();
    const avatar = URL + data_avatar.attr('src');

    data.push({
      avatar,
      title,
      URL_title,
      date,
      seen,
      des,
    });
  }

  data.push({
    title: 'Xem thêm',
    URL_title: newsURL,
  });

  page_news = {
    1: data,
  }

  return page_news;
};

const crawlDataVinhLong = async (req, res) => {
  const { contentPage, page } = req?.body;
  if (!contentPage) {
    const results = await Promise.all([bodCrawler(), structureCrawler(), wsCrawler(), newsCrawler(newsURL, 1), notificationCrawler(ntURL, 1)]);

    const information = {
      bod: results[0],
      structure: results[1],
      workingSchedule: results[2],
      news: results[3],
      notifications: results[4]
    };
    res.send(information).status(200);
  }
  else {
    res.send(information).status(200);
  }
}

module.exports = crawlDataVinhLong;