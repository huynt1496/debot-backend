const rp = require("request-promise");
const cheerio = require("cheerio");

const bodURL = 'https://hcm.edu.vn/co-cau-to-chuc/cctc/1';
const structureURL = 'https://hcm.edu.vn/so-luoc-ve-nganh-gddt-tphcm/nganh-giao-duc-va-dao-tao-thanh-pho-ho-chi-minh/ct/31857/54481';
const wsURL = 'https://hcm.edu.vn/lich-cong-tac-tuan/c/39762';
const ntURL = 'https://hcm.edu.vn/thongbao';
const newsURL = 'https://hcm.edu.vn/tin-tuc/c/7953';


const options = (url, page) => {
  let newUrl = url;
  if (page) {
    newUrl = `${url}/${page}`;
  }
  return {
    uri: newUrl,
    transform: (body) => cheerio.load(body)
  }
};

const bodCrawler = async () => {
  try {
    var $ = await rp(options(bodURL));
  } catch (error) {
    return error;
  }

  let data = [];
  for (let i = 2; i < 6; i++) {
    const col2 = $(`#MainContent > div.d-flex.border.w-100:nth-child(${2 * i + 2}) >div >.media>img`);
    const col3 = $(`div#MainContent > div.d-flex.border.w-100:nth-child(${2 * i + 2}) > div > div.media > div.media-body>div`);
    const title = $(`div#MainContent > h4.font-weight-bold.pl-2.mt-2:nth-child(${2 * i + 1})`).text().trim() + `:  ` + $(`div#MainContent > div.d-flex.border.w-100:nth-child(${2 * i + 2}) > div > div.media > div.media-body>h5`).text().trim();
    const info = col3.text().trim();
    const avatar = col2.attr('src');

    data.push({
      avatar,
      info,
      title,
    });
  }
  return data;
};

const wsCrawler = async (page) => {
  try {
    var $ = await rp(options(wsURL, page));
  } catch (error) {
    return error;
  }
  const wsContent = $("#ContentPlaceHolder1_MainContent_chuyenmuc_pnNew > div.row");
  var page_ws = {};
  let data = [];
  for (let i = 0; i < wsContent.length; i++) {
    const title = $(`#ContentPlaceHolder1_MainContent_chuyenmuc_pnNew > div.row:nth-child(${i + 1}) > div > a >div >h5`).text().trim();
    const URL_title = "https://hcm.edu.vn/" + $(`#ContentPlaceHolder1_MainContent_chuyenmuc_pnNew > div.row:nth-child(${i + 1}) > div > a`).attr('href');
    const date = $(`#ContentPlaceHolder1_MainContent_chuyenmuc_pnNew > div.row:nth-child(${i + 1}) > div > a >div >p`).text().trim();

    data.push({
      // avatar,
      title,
      URL_title,
      // user,
      date,
      // seen,
      // des
    });
  }
  if (page == '1') {

    const valueMaxPage = Number(25) || 1;
    page_ws = {
      [page]: data,
      totalPage: valueMaxPage
    }
  }
  else {
    page_ws = {
      [page]: data,
    }
  }
  return page_ws;
};

const structureCrawler = async () => {
  try {
    var $ = await rp(options(structureURL));
  } catch (error) {
    return error;
  }

  const digContent = $("#ContentPlaceHolder1_tinchitiet_pnNew>div");
  const dataDigContent = {

    data: digContent.html(),
    type: "html",
  };
  return dataDigContent;
};

const notificationCrawler = async (page) => {
  try {
    var $ = await rp(options(ntURL, page));
  } catch (error) {
    return error;
  }
  const notificationContent = $("#ContentPlaceHolder1_MainContent_ctl00_UpdatePanel1 > div > div > table >tbody > tr");
  var page_notification = {}
  let data = [];
  for (let i = 1; i < notificationContent.length - 1; i++) {

    const title = $(`#ContentPlaceHolder1_MainContent_ctl00_UpdatePanel1 > div > div > table >tbody > tr:nth-child(${i + 1}) > td:nth-child(2) > div >div >a >div >h5 `).text().trim();
    const URL_title = "https://hcm.edu.vn/" + $(`#ContentPlaceHolder1_MainContent_ctl00_UpdatePanel1 > div > div > table >tbody > tr:nth-child(${i + 1}) > td:nth-child(2) > div >div >a`).attr('href');
    const date = $(`#ContentPlaceHolder1_MainContent_ctl00_UpdatePanel1 > div > div > table >tbody > tr:nth-child(${i + 1}) > td:nth-child(2) > div >div >a >div >p `).text().trim();
    data.push({
      // avatar,
      title,
      URL_title,
      // user,
      date,
      // seen,
    });
  }
  if (page == '1') {
    // const stringPage = $("#ContentPlaceHolder1_MainContent_ctl00_UpdatePanel1 > div > div > table >tbody > tr.pagination-ys > td").text().trim();
    // const index = stringPage.lastIndexOf('/ ');
    const valueMaxPage = Number(25) || 1;
    page_notification = {
      [page]: data,
      totalPage: valueMaxPage
    }
  }
  else {
    page_notification = {
      [page]: data,
    }
  }
  return page_notification;
};

const newsCrawler = async (page) => {
  try {
    var $ = await rp(options(newsURL, page));
  } catch (error) {
    return error;
  }
  const newsContent = $("#ContentPlaceHolder1_MainContent_chuyenmuc_pnNew > div");
  var page_news = {}
  let data = [];
  for (let i = 0; i < newsContent.length - 1; i++) {
    const data_avatar = $(`#ContentPlaceHolder1_MainContent_chuyenmuc_pnNew > div:nth-child(${i + 1}) >div.col-sm-5.px-0  >a >img`);

    const title = $(`#ContentPlaceHolder1_MainContent_chuyenmuc_pnNew > div:nth-child(${i + 1})>div:last-child > a>div>h5`).text().trim();


    const URL_title = "https://hcm.edu.vn/" + $(`#ContentPlaceHolder1_MainContent_chuyenmuc_pnNew > div:nth-child(${i + 1})>div:last-child > a`).attr('href');

    const date = $(`#ContentPlaceHolder1_MainContent_chuyenmuc_pnNew > div:nth-child(${i + 1})>div:last-child > a>div>p:nth-child(2)`).text().trim();

    const des = $(`#ContentPlaceHolder1_MainContent_chuyenmuc_pnNew > div:nth-child(${i + 1})>div:last-child > a>div>p:nth-child(3)`).text() || "[...]";
    const avatar = data_avatar.attr('src');
    data.push({
      avatar,
      title,
      URL_title,
      // user,
      date,
      des
    });
  }

  if (page == '1') {

    const valueMaxPage = Number(25) || 1;
    page_news = {
      [page]: data,
      totalPage: valueMaxPage
    }
  }
  else {
    page_news = {
      [page]: data,
    }
  }


  return page_news;
};

const crawlDataHCM = async (req, res) => {
  const { contentPage, page } = req?.body;
  if (!contentPage) {
    const results = await Promise.all([bodCrawler(), structureCrawler(), wsCrawler(1), newsCrawler(1), notificationCrawler(1)]);

    const information = {
      bod: results[0],
      structure: results[1],
      workingSchedule: results[2],
      news: results[3],
      notifications: results[4]
    };
    res.send(information).status(200);
  } else {
    if (contentPage == 'ws') {
      const workingScheduleData = await wsCrawler(page);
      const information = {
        workingSchedule: workingScheduleData,
      };
      res.send(information).status(200);
    }
    if (contentPage == 'news') {
      const newsData = await newsCrawler(page);

      const information = {
        news: newsData,
      };
      res.send(information).status(200);
    }
    if (contentPage == 'nt') {
      const notificationData = await notificationCrawler(page);

      const information = {
        notifications: notificationData,
      };
      res.send(information).status(200);
    }
  }
}

module.exports = crawlDataHCM;