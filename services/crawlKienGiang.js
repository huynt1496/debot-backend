const rp = require("request-promise");
const cheerio = require("cheerio");
const puppeteer = require("puppeteer")

const bodURL = 'http://sgddt.kiengiang.gov.vn/trang/TinTuc/102/497/Thong-tin-lanh-dao';
const structureURL = 'https://sgddt.kiengiang.gov.vn/trang/TinTuc/112/908/So-do-to-chuc.html';
const wsURL = 'http://sgddt.kiengiang.gov.vn/trang/TinTuc/tinchuyenmuc.aspx?chuyenmuc=28';
const ntURL = 'http://sgddt.kiengiang.gov.vn/trang/TinTuc/tinchuyenmuc.aspx?chuyenmuc=30';
const newsURL = 'https://sgddt.kiengiang.gov.vn/trang/TinTuc/tinchuyenmuc.aspx?chuyenmuc=127';


const options = async (url) => {

  const browser = await puppeteer.launch();
  const newPage = await browser.newPage();
  await newPage.goto(url);

  const datatotalPage = await newPage.evaluate(() => document.body.outerHTML);
  var $ = cheerio.load(datatotalPage);
  await browser.close();

  return $;

};

const bodCrawler = async () => {
  try {
    var $ = await options(bodURL);
  } catch (error) {
    return error;
  }
  const bodContent = $("#left > div > div > div > div > div.textbody > div > table > tbody > tr");
  let data = [];
  for (let i = 0; i < bodContent.length; i++) {
    if (i == 3) {
      const URL = "http://sgddt.kiengiang.gov.vn";
      const col2 = $(`#left > div > div > div > div > div.textbody > div > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(1) > img`);
      const col3 = $(`#left > div > div > div > div > div.textbody > div > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(2)`);
      const info = col3.text().trim();
      const avatar = URL + col2.attr('src');
      data.push({
        avatar,
        info,
      });
    } else {
      const URL = "http://sgddt.kiengiang.gov.vn";
      const col2 = $(`#left > div > div > div > div > div.textbody > div > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(1) > p > img`);
      const col3 = $(`#left > div > div > div > div > div.textbody > div > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(2)`);
      const info = col3.text().trim();
      const avatar = URL + col2.attr('src');
      data.push({
        avatar,
        info,
      });
    }
  }
  return data;
};

const wsCrawler = async () => {
  try {
    var $ = await options(wsURL);
  } catch (error) {
    return error;
  }
  const wsContent = $("#ChuyenMuc > div > ul > li");
  var page_ws = {};
  let data = [];
  for (let i = 0; i < wsContent.length; i++) {
    const URL = "http://sgddt.kiengiang.gov.vn";
    const data_avatar = $(`#ChuyenMuc > div > ul > li:nth-child(${i + 1}) > a > img`);
    const title = $(`#ChuyenMuc > div > ul > li:nth-child(${i + 1}) > a > h2`).text().trim();
    const URL_title = $(`#ChuyenMuc > div > ul > li:nth-child(${i + 1}) > a`).attr('href');
    const date = $(`#ChuyenMuc > div > ul > li:nth-child(${i + 1}) > span`).text().trim();
    const avatar = URL + data_avatar.attr('src');
    data.push({
      avatar,
      title,
      URL_title,
      date
    });
  }
  page_ws = {
    1: data,
  }

  return page_ws;
};

const structureCrawler = async () => {
  try {
    var $ = await options(structureURL);
  } catch (error) {
    return error;
  }
  const digContent = $("#content > div.textbody > div > div");

  const dataDigContent = {
    data: digContent.html(),
    type: "html",
  };
  return dataDigContent;
};

const notificationCrawler = async () => {
  try {
    var $ = await options(ntURL);
  } catch (error) {
    return error;
  }
  const notificationContent = $("#ChuyenMuc > div > ul > li");
  var page_notification = {}
  let data = [];
  for (let i = 0; i < notificationContent.length; i++) {
    const URL = "http://sgddt.kiengiang.gov.vn";
    const data_avatar = $(`#ChuyenMuc > div > ul > li:nth-child(${i + 1}) > a > img`);
    const title = $(`#ChuyenMuc > div > ul > li:nth-child(${i + 1}) > a > h2`).text().trim();
    const URL_title = $(`#ChuyenMuc > div > ul > li:nth-child(${i + 1}) > a`).attr('href');
    const date = $(`#ChuyenMuc > div > ul > li:nth-child(${i + 1}) > span`).text().trim();
    const avatar = URL + data_avatar.attr('src');
    data.push({
      avatar,
      title,
      URL_title,
      date
    });
  }
  page_notification = {
    1: data,
  }

  return page_notification;
};

const newsCrawler = async () => {
  try {
    var $ = await options(newsURL);
  } catch (error) {
    return error;
  }
  const newsContent = $("#ChuyenMuc > div > ul > li");
  let data = [];
  for (let i = 0; i < newsContent.length; i++) {
    const URL = "http://sgddt.kiengiang.gov.vn";
    const data_avatar = $(`#ChuyenMuc > div > ul > li:nth-child(${i + 1}) > a > img`);
    const title = $(`#ChuyenMuc > div > ul > li:nth-child(${i + 1}) > a > h2`).text().trim();
    const URL_title = $(`#ChuyenMuc > div > ul > li:nth-child(${i + 1}) > a`).attr('href');
    const date = $(`#ChuyenMuc > div > ul > li:nth-child(${i + 1}) > span`).text().trim();
    const avatar = URL + data_avatar.attr('src');
    data.push({
      avatar,
      title,
      URL_title,
      date
    });
  }
  page_news = {
    1: data,
  }

  return page_news;
};

const crawlKienGiang = async (req, res) => {
  const { contentPage, page } = req?.body;
  if (!contentPage) {
    const results = await Promise.all([bodCrawler(), structureCrawler(), wsCrawler(), newsCrawler(), notificationCrawler()]);

    const information = {
      bod: results[0],
      structure: results[1],
      workingSchedule: results[2],
      news: results[3],
      notifications: results[4]
    };
    res.send(information).status(200);
  } else {
    res.send([]).status(200);
  }
}

module.exports = crawlKienGiang;