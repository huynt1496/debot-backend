require('dotenv-flow').config();

const bodyParser = require('body-parser');
const express = require('express');

const app = express();

const cors = require('cors');
const path = require('path');
const http = require('http');

app.use(cors());
app.use(bodyParser.urlencoded({
  extended: false, limit: '50mb',
}));
app.use(bodyParser.json({
  extended: false, limit: '50mb',
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/crawl', require('./api/crawl'));

const PORT = process.env.PORT || 1235;
// start http server
if (process.env.NODE_ENV === 'development') {
  const PORT_HTTP = 1235;
  http.createServer(app).listen(PORT_HTTP, () => {
    console.log(`Server API listening on port ${PORT_HTTP}`);
  });
}
