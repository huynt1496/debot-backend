const crawlDataDakLak = require('../../services/crawlDataDakLak');
const crawlDataLamDong = require('../../services/crawlDataLamDong');
const crawlDakNong = require('../../services/crawlDakNong');
const crawlKhanhHoa = require('../../services/crawlKhanhHoa');
const crawlDataDongNai = require('../../services/crawlDataDongNai');
const crawlNinhThuan = require('../../services/crawlNinhThuan');
const crawlDataTraVinh = require('../../services/crawlDataTraVinh');
const crawlDataHCM = require('../../services/crawlDataHCM');
const crawlDataKonTum = require('../../services/crawDataKonTum');
const crawlDataAnGiang = require('../../services/crawDataAnGiang');
const crawlBinhDuong = require('../../services/crawlBinhDuong');
const crawlDataGiaLai = require('../../services/crawlDataGiaLai');
const crawlDataCanTho = require('../../services/crawlDataCanTho');
const crawlDataVinhLong = require('../../services/crawlDataVinhLong');
const crawlDataDongThap = require('../../services/crawlDataDongThap');
const crawlDataBRVT = require('../../services/crawlDataBRVT');
const crawlDataTayNinh = require('../../services/crawlDataTayNinh');
const crawlDataBinhThuan = require('../../services/crawlDataBinhThuan');
const crawlKienGiang = require('../../services/crawlKienGiang');
const crawlTienGiang = require('../../services/crawlTienGiang');
const crawlDataBinhPhuoc = require('../../services/crawlDataBinhPhuoc');

const crawl = require('express').Router();

crawl.post('/department', async (req, res) => {
  console.log('/department', req.body);
  const {
    province,
  } = req.body;
  switch (province) {
    case '03':
      crawlDataHCM(req, res);
      break;

    case '56':
      crawlKhanhHoa(req, res);
      break;

    case '58':
      crawlNinhThuan(req, res);
      break;

    case '60':
      crawlDataBinhThuan(req, res);
      break;

    case '62':
      crawlDataKonTum(req, res);
      break;
      
    case '64':
      crawlDataGiaLai(req, res);
      break;

    case '66':
      crawlDataDakLak(req, res);
      break;

    case '67':
      crawlDakNong(req, res);
      break;

    case '68':
      crawlDataLamDong(req, res);
      break;

    case '74':
      crawlBinhDuong(req, res);
      break;

    case '75':
      crawlDataDongNai(req, res);
      break;

    case '82':
      crawlTienGiang(req, res);
      break;

    case '77':
      crawlDataBRVT(req, res);
      break;

    case '84':
      crawlDataTraVinh(req, res);
      break;

    case '86':
      crawlDataVinhLong(req, res);
      break;

    case '87':
      crawlDataDongThap(req, res);
      break;

    case '89':
      crawlDataAnGiang(req, res);
      break;

    case '91':
      crawlKienGiang(req, res);
      break;

    case '92':
      crawlDataCanTho(req, res);
      break;

    case '72':
      crawlDataTayNinh(req, res);
      break;

    case '70':
      crawlDataBinhPhuoc(req, res);
      break;

    default:
      res.send().status(200);
      break;
  }
});

module.exports = crawl;
